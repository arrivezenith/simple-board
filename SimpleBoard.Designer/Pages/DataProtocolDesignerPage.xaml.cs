﻿using SimpleBoard.DependencyInjection;
using SimpleBoard.Designer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SimpleBoard.Designer.Pages
{
    /// <summary>
    /// DataProtocolDesignerPage.xaml 的交互逻辑
    /// </summary>
    [InjectAs(InjectionType.Singleton)]
    public partial class DataProtocolDesignerPage : Page
    {
        public DataProtocolDesignerPage(DataProtocolDesignerPageViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;
            this.viewModel = viewModel;
        }

        private readonly DataProtocolDesignerPageViewModel viewModel;

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            this.viewModel.AddUnit();
        }
    }
}
