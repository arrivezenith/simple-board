﻿using SimpleBoard.Designer.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SimpleBoard.Designer.Pages
{
    /// <summary>
    /// DataSourcePanel.xaml 的交互逻辑
    /// </summary>
    [DependencyInjection.InjectAs(DependencyInjection.InjectionType.Singleton)]
    public partial class DataSourcePanel : Page
    {
        public DataSourcePanel(DataProtocolDesignerPageViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;
            this.viewModel = viewModel;
        }

        private readonly DataProtocolDesignerPageViewModel viewModel;

        private void DataUnitList_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && DataUnitList.SelectedItems.Count > 0)
            {
                DragDrop.DoDragDrop(this, DataUnitList.SelectedItem, DragDropEffects.Move);
            }
        }
    }
}
