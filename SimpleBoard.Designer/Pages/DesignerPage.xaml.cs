﻿using Microsoft.Toolkit.Mvvm.Messaging;
using SimpleBoard.ControllerMeta;
using SimpleBoard.DependencyInjection;
using SimpleBoard.Designer.Extensions;
using SimpleBoard.Designer.Messages;
using SimpleBoard.Designer.Services;
using SimpleBoard.Designer.ViewModels;
using SimpleBoard.Project.UI;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SimpleBoard.Designer.Pages
{
    /// <summary>
    /// DesignerPage.xaml 的交互逻辑
    /// </summary>
    [InjectAs(InjectionType.Singleton)]
    public partial class DesignerPage : Page
    {
        public DesignerPage(DesignerPageViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;
            this.viewModel = viewModel;

            this.scaleOptions.SelectedIndex = 3;

            DragDrop.AddDropHandler(Container, (s, e) =>
            {
                if (!e.Data.GetDataPresent(typeof(ControllerInfo))) return;
                var controller = (ControllerInfo)e.Data.GetData(typeof(ControllerInfo));
                var instance = Container.AddController(controller);
                instance.AddDragZoomBehavior((s, e) =>
                {
                    WeakReferenceMessenger.Default.Send(new ActiveElementChangedMessage(instance));
                });
            });
        }

        public void LoadPage(PageInfo pageInfo)
        {
            var controlService = App.AutoWired<ControllerService>();
            // 添加控件
            foreach (var controlInfo in pageInfo.Controls)
            {
                var controlMeta = controlService.Controllers.Where(x => x.AssemblyFullName == controlInfo.AssemblyFullName)
                                                            .Where(x => x.TypeFullName == controlInfo.TypeFullName)
                                                            .Single();
                var element = Container.AddController(controlMeta);
                element.AddDragZoomBehavior((s, e) =>
                {
                    WeakReferenceMessenger.Default.Send(new ActiveElementChangedMessage(element));
                });
                controlService.SetWidth(element, controlInfo.Width);
                controlService.SetHeight(element, controlInfo.Height);
                controlService.SetLeft(element, controlInfo.Left);
                controlService.SetTop(element, controlInfo.Top);

                foreach (var prop in controlInfo.Properties)
                {
                    var valueStr = prop.Value.ToString()!;
                    var valueType = controlMeta.Properties.Where(x => x.Name == prop.Key).Single().Type;
                    object value = valueType switch
                    {
                        PropertyType.String => valueStr,
                        PropertyType.Int32 => int.Parse(valueStr),
                        PropertyType.Double => double.Parse(valueStr),
                        PropertyType.Bool => bool.Parse(valueStr),
                        _ => throw new InvalidOperationException()
                    };
                    element.SetPropertyValue(prop.Key, value);
                }
            }
            // 設置ViewModel
            this.viewModel.GridWidth = pageInfo.Width;
            this.viewModel.GridHeight = pageInfo.Height;
            this.viewModel.GridBackground = new SolidColorBrush((Color)ColorConverter.ConvertFromString(pageInfo.BackgroundColor));
        }

        private readonly DesignerPageViewModel viewModel;

        protected override void OnRenderSizeChanged(SizeChangedInfo sizeInfo)
        {
            base.OnRenderSizeChanged(sizeInfo);
            viewModel.UpdateCanvasSize((int)canvas.ActualWidth, (int)canvas.ActualHeight);
        }
    }
}
