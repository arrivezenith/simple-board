﻿using DSF.Net.Controllers.Theme;
using SimpleBoard.ControllerMeta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DSF.Net.Controllers.Process
{
    /// <summary>
    /// Battery.xaml 的交互逻辑
    /// </summary>
    [SimpleBoardController("DSF仪表-电池")]
    [HasConfigurableProperty(nameof(Value), PropertyType.Double, "数值")]
    public partial class Battery : UserControl,IUserTheme
    {
        public Battery()
        {
            InitializeComponent();
            DataContext = this;
            Width = 148;
            Height = 315;
        }

        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Value.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(double), typeof(Battery), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as Battery;
                var oldValue = (double)e.OldValue;
                var newValue = (double)e.NewValue;
                var animation = new DoubleAnimation
                {
                    From = oldValue,
                    To = newValue,
                    Duration = new Duration(new TimeSpan(0, 0, 0, 0, 500)),
                    EasingFunction = new QuadraticEase()
                };
                c.BeginAnimation(AnimateProperty, animation);
            }));

        private static readonly DependencyProperty AnimateProperty =
            DependencyProperty.Register("Animate", typeof(double), typeof(Battery), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as Battery;
                var value = (double)e.NewValue;
                c.ValueGrid.Height = value / 100 * 256;
            }));

        public Theme.Colors ThemeColor
        {
            get { return (Theme.Colors)GetValue(ThemeColorProperty); }
            set { SetValue(ThemeColorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ThemeColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ThemeColorProperty =
            DependencyProperty.Register("ThemeColor", typeof(Theme.Colors), typeof(Battery), new PropertyMetadata(Theme.Colors.Default));

    }
}
