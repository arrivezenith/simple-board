﻿using DSF.Net.Controllers.Theme;
using SimpleBoard.ControllerMeta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DSF.Net.Controllers.Process
{
    /// <summary>
    /// RotateProcess.xaml 的交互逻辑
    /// </summary>
    [SimpleBoardController("DSF指示器-圆形文本")]
    [HasConfigurableProperty(nameof(Value), PropertyType.Double, "数值")]
    [HasConfigurableProperty(nameof(Text), PropertyType.String, "文本")]
    [HasConfigurableProperty(nameof(Precision), PropertyType.Int32, "保留小数")]
    public partial class RotateProcess : UserControl,IUserTheme
    {
        public RotateProcess()
        {
            InitializeComponent();
            DataContext = this;
            Width = 72;
            Height = 72;
        }
        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Value.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(double), typeof(RotateProcess), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as RotateProcess;
                var oldValue = (double)e.OldValue;
                var newValue = (double)e.NewValue;
                var animation = new DoubleAnimation
                {
                    From = oldValue,
                    To = newValue,
                    Duration = new Duration(new TimeSpan(0, 0, 0, 0, 500)),
                    EasingFunction = new QuadraticEase()
                };
                c.BeginAnimation(ArcAnimateProperty, animation);
                c.BeginAnimation(TextAnimateProperty, animation);
            }));


        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(RotateProcess), new PropertyMetadata("Process", (s, e) =>
            {
                var c = s as RotateProcess;
                c.labelText.Content = e.NewValue;
            }));
        public int Precision
        {
            get { return (int)GetValue(PrecisionProperty); }
            set { SetValue(PrecisionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Precision.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PrecisionProperty =
            DependencyProperty.Register("Precision", typeof(int), typeof(RotateProcess), new PropertyMetadata(2, (s, e) => {
                var c = s as RotateProcess;
                var value = (int)e.NewValue;
                var priciseStr = new StringBuilder("#0.");
                for (var i = 0; i < value; i++)
                {
                    priciseStr.Append('0');
                }
                c.label.Content = c.Value.ToString(priciseStr.ToString());
            }));

        private static readonly DependencyProperty ArcAnimateProperty =
            DependencyProperty.Register("ArcAnimate", typeof(double), typeof(RotateProcess), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as RotateProcess;
                var value = (double)e.NewValue;
                c.arc.EndAngle = value / 100 * 360;
            }));
        private static readonly DependencyProperty TextAnimateProperty =
            DependencyProperty.Register("TextAnimate", typeof(double), typeof(RotateProcess), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as RotateProcess;
                var value = (double)e.NewValue;
                var priciseStr = new StringBuilder("#0.");
                for (var i = 0; i < c.Precision; i++)
                {
                    priciseStr.Append('0');
                }
                c.label.Content = value.ToString(priciseStr.ToString());
            }));
        public Theme.Colors ThemeColor
        {
            get { return (Theme.Colors)GetValue(ThemeColorProperty); }
            set { SetValue(ThemeColorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ThemeColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ThemeColorProperty =
            DependencyProperty.Register("ThemeColor", typeof(Theme.Colors), typeof(RotateProcess), new PropertyMetadata(Theme.Colors.Default));

    }
}
