﻿using SimpleBoard.ControllerMeta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SimpleBoard.Designer.Extensions
{
    public static class PanelExtension
    {
        private static int controlId = 0;
        public static FrameworkElement AddController(this Panel panel, ControllerInfo controller)
        {
            var instanse = (FrameworkElement)Activator.CreateInstance(controller.AssemblyFullName, controller.TypeFullName)!.Unwrap()!;
            panel.Children.Add(instanse);
            instanse.Name = $"control{controlId++}";
            return instanse;
        }
    }
}
