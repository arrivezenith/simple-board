﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSF.Net.Controllers.Ratio
{
    public enum HoriRatioStatus
    {
        LeftSelected,
        RightSelected
    } 

    public enum VertiRatioStatus
    {
        UpSelected,
        DownSelected
    }
}
