﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBoard.Designer.ViewModels
{
    public class ScaleOptionViewModel
    {
        public string DisplayName { get; set; } = null!;
        public double Value { get; set; }

        public override string ToString()
        {
            return DisplayName;
        }
    }
}
