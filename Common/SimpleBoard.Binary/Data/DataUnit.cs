﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBoard.Binary.Data
{
    public class DataUnit
    {
        public string Name { get; set; } = null!;
        public DataUnitType Type { get; set; }
    }
}
