﻿using DSF.Net.Controllers.Theme;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DSF.Net.Controllers.Switch
{
    /// <summary>
    /// TitledSwitch2.xaml 的交互逻辑
    /// </summary>
    public partial class TitledSwitch2 : UserControl, IUserTheme
    {
        public TitledSwitch2()
        {
            InitializeComponent();
            DataContext = this;
            LabelOn.MouseLeftButtonDown += Touch_MouseLeftButtonDown;
            LabelOff.MouseLeftButtonDown += Touch_MouseLeftButtonDown;
        }

        public event EventHandler ValueChanged;

        private void Touch_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Value = sender.Equals(LabelOn) ? SwitchStatus.On : SwitchStatus.Off;
            ValueChanged?.Invoke(this, null);
        }

        /// <summary>
        /// 获取/设置控件上显示的文本
        /// </summary>
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        /// <summary>
        /// 获取/设置状态为On时的文本
        /// </summary>
        public string TextOn
        {
            get { return (string)GetValue(TextOnProperty); }
            set { SetValue(TextOnProperty, value); }
        }

        /// <summary>
        /// 获取/设置状态为Off时的文本
        /// </summary>
        public string TextOff
        {
            get { return (string)GetValue(TextOffProperty); }
            set { SetValue(TextOffProperty, value); }
        }

        public SwitchStatus Value
        {
            get { return (SwitchStatus)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public Theme.Colors ThemeColor
        {
            get { return (Theme.Colors)GetValue(ThemeColorProperty); }
            set { SetValue(ThemeColorProperty, value); }
        }

        public static readonly DependencyProperty ThemeColorProperty =
            DependencyProperty.Register("ThemeColor", typeof(Theme.Colors), typeof(TitledSwitch2), new PropertyMetadata(Theme.Colors.Default));


        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(SwitchStatus), typeof(TitledSwitch2), new PropertyMetadata(SwitchStatus.On, (s, e) =>
            {
                var c = s as TitledSwitch2;
                var value = (SwitchStatus)e.NewValue;
                c.LabelOn.Foreground = value == SwitchStatus.On ? c.ThemeColor.TextEnabled : c.ThemeColor.TextDisabled;
                c.LabelOff.Foreground = value == SwitchStatus.Off ? c.ThemeColor.TextEnabled : c.ThemeColor.TextDisabled;
                c.RectOn.Fill = value == SwitchStatus.On ? c.ThemeColor.Light : c.ThemeColor.Dark;
                c.RectOff.Fill = value == SwitchStatus.Off ? c.ThemeColor.Light : c.ThemeColor.Dark;
            }));
        public static readonly DependencyProperty TextOnProperty =
            DependencyProperty.Register("TextOn", typeof(string), typeof(TitledSwitch2), new PropertyMetadata("On", (s, e) =>
            {
                var c = s as TitledSwitch2;
                c.LabelOn.Content = e.NewValue;
            }));
        public static readonly DependencyProperty TextOffProperty =
            DependencyProperty.Register("TextOff", typeof(string), typeof(TitledSwitch2), new PropertyMetadata("Off", (s, e) =>
            {
                var c = s as TitledSwitch2;
                c.LabelOff.Content = e.NewValue;
            }));
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(TitledSwitch2), new PropertyMetadata("TitledSwitch", (s, e) =>
            {
                var c = s as TitledSwitch2;
                c.TitleLabel.Content = e.NewValue;
            }));
    }
}
