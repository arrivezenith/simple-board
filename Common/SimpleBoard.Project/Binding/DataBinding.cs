﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBoard.Project.Binding
{
    public class DataBinding
    {
        public string ControlName { get; set; } = null!;
        public string PropertyName { get; set; } = null!;
        public string DataUnitName { get; set; } = null!;
    }
}
