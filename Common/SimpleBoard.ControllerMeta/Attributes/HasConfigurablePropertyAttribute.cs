﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBoard.ControllerMeta
{
    [AttributeUsage(AttributeTargets.Class, AllowMultiple = true)]
    public class HasConfigurablePropertyAttribute : Attribute
    {
        public HasConfigurablePropertyAttribute(string propertyName, PropertyType propertyType, string title)
        {
            this.PropertyName = propertyName;
            this.PropertyType = propertyType;
            this.Title = title;
        }

        public string PropertyName { get; set; }
        public PropertyType PropertyType { get; set; }
        public string Title { get; set; }
    }
}
