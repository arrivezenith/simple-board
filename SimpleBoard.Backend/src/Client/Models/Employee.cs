﻿namespace SimpleBoard.Backend.Client.Models
{
    public class Employee
    {
        /// <summary>
        /// 员工工号
        /// </summary>
        public int EmployeeId { get; set; }

        /// <summary>
        /// 员工姓名
        /// </summary>
        public string Name { get; set; } = null!;

        /// <summary>
        /// 联系方式
        /// </summary>
        public string Contact { get; set; } = null!;

        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; } = null!;
    }
}
