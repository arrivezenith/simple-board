﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBoard.Compiler
{
    public class CompileResult
    {
        public CompileResult(string pageGeneratorAssemblyPath, string dataProtocalJsonPath)
        {
            this.DataProtocolJsonPath = dataProtocalJsonPath;
            this.PageGeneratorAssemblyPath = pageGeneratorAssemblyPath;
        }

        public string PageGeneratorAssemblyPath { get; set; } = null!;

        public string DataProtocolJsonPath { get; set; } = null!;
    }
}
