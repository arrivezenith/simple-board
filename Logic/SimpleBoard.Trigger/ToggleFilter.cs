﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBoard.Trigger
{
    public class ToggleFilter : Filter
    {
        private bool? previousValue; 

        public override bool IsFitCondition(object propertyValue)
        {
            var value = (bool)propertyValue;
            if (previousValue is null) previousValue = value;
            var result = value != previousValue;

            previousValue = value;
            return result;
        }
    }
}
