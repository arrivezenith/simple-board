﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DSF.Net.Controllers.Ratio
{
    /// <summary>
    /// HoriRatioxaml.xaml 的交互逻辑
    /// </summary>
    public partial class HoriRatioxaml : UserControl
    {
        public HoriRatioxaml()
        {
            InitializeComponent();
            labelLeft.MouseLeftButtonDown += Touch;
            labelRight.MouseLeftButtonDown += Touch;
            DataContext = this;
        }

        public event EventHandler ValueChanged;

        private void Touch(object sender, MouseButtonEventArgs e)
        {
            Value = sender.Equals(labelLeft) ? HoriRatioStatus.LeftSelected : HoriRatioStatus.RightSelected;
            ValueChanged?.Invoke(this, null);
        }

        public string TextLeft
        {
            get { return (string)GetValue(TextLeftProperty); }
            set { SetValue(TextLeftProperty, value); }
        }

        public string TextRight
        {
            get { return (string)GetValue(TextRightProperty); }
            set { SetValue(TextRightProperty, value); }
        }

        public HoriRatioStatus Value
        {
            get { return (HoriRatioStatus)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public static readonly DependencyProperty TextLeftProperty =
            DependencyProperty.Register("TextLeft", typeof(string), typeof(HoriRatioxaml), new PropertyMetadata("FINISHED", (s, e) =>
            {
                var c = s as HoriRatioxaml;
                c.labelLeft.Content = e.NewValue;
            }));
        public static readonly DependencyProperty TextRightProperty =
            DependencyProperty.Register("TextRight", typeof(string), typeof(HoriRatioxaml), new PropertyMetadata("UNFINISHED", (s, e) =>
            {
                var c = s as HoriRatioxaml;
                c.labelRight.Content = e.NewValue;
            }));
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(HoriRatioStatus), typeof(HoriRatioxaml), new PropertyMetadata(HoriRatioStatus.RightSelected, (s, e) =>
            {
                var c = s as HoriRatioxaml;
                var value = (HoriRatioStatus)e.NewValue;
                var storyboard = value == HoriRatioStatus.LeftSelected ? (c.Resources["SelectToLeft"] as Storyboard) : (c.Resources["SelectToRight"] as Storyboard);
                storyboard.Begin();
            }));
    }
}
