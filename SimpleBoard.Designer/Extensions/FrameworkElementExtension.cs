﻿using SimpleBoard.Behavior;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SimpleBoard.Designer.Extensions
{
    public static class FrameworkElementExtension
    {
        public static void AddDragZoomBehavior(this FrameworkElement element, EventHandler mouseEventFinishedHandler)
        {
            var behavior = new TranslateZoomBehavior();
            behavior.MouseEventFinished += mouseEventFinishedHandler;
            Microsoft.Xaml.Behaviors.Interaction.GetBehaviors(element).Add(behavior);
        }
    }
}
