﻿using DSF.Net.Controllers.Ratio;
using DSF.Net.Controllers.Theme;
using SimpleBoard.ControllerMeta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DSF.Net.Controllers.Process
{
    /// <summary>
    /// HoriProcess.xaml 的交互逻辑
    /// </summary>
    [SimpleBoardController("DSF指示器-水平单区间文本")]
    [HasConfigurableProperty(nameof(Value), PropertyType.Double, "数值")]
    [HasConfigurableProperty(nameof(Limit), PropertyType.Double, "区间长度")]
    public partial class HoriProcess : UserControl,IUserTheme
    {
        public HoriProcess()
        {
            InitializeComponent();
            DataContext = this;
            Width = 210;
            Height = 10;
        }


        public double Limit
        {
            get { return (double)GetValue(LimitProperty); }
            set { SetValue(LimitProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Limit.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LimitProperty =
            DependencyProperty.Register("Limit", typeof(double), typeof(HoriProcess), new PropertyMetadata(100d, (s, e) =>
            {
                var c = s as HoriProcess;
                var limit = (double)e.NewValue;
                if (c.Value > 0)
                {
                    c.blueBar.Width = 100 * c.Value / limit;
                }
                else if (c.Value < 0)
                {
                    c.yellowBar.Height = 100 * -c.Value / limit;
                }
            }));


        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Value.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(double), typeof(HoriProcess), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as HoriProcess;
                var oldValue = (double)e.OldValue;
                var newValue = (double)e.NewValue;
                var animation = new DoubleAnimation
                {
                    From = oldValue,
                    To = newValue,
                    Duration = new Duration(new TimeSpan(0, 0, 0, 0, 500)),
                    EasingFunction = new QuadraticEase()
                };
                c.BeginAnimation(AnimateProperty, animation);
            }));

        private static readonly DependencyProperty AnimateProperty =
            DependencyProperty.Register("Animate", typeof(double), typeof(HoriProcess), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as HoriProcess;
                var value = (double)e.NewValue;
                c.blueBar.Width = value > 0 ? 100 * value / c.Limit : 0;
                c.yellowBar.Width = value < 0 ? 100 * -value / c.Limit : 0;
            }));

        public Theme.Colors ThemeColor
        {
            get { return (Theme.Colors)GetValue(ThemeColorProperty); }
            set { SetValue(ThemeColorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ThemeColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ThemeColorProperty =
            DependencyProperty.Register("ThemeColor", typeof(Theme.Colors), typeof(HoriProcess), new PropertyMetadata(Theme.Colors.Default));


    }
}
