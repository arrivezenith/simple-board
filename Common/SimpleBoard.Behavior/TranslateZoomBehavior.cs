using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using Microsoft.Xaml.Behaviors;

namespace SimpleBoard.Behavior
{
    public class TranslateZoomBehavior : Behavior<FrameworkElement>
    {
        private bool isDragging = false;
        private bool isAdjustingTranslate = false;
        private bool isAdjustingZoom = false;

        private Point lastMousePoint;
        private Transform cachedRenderTransform = null!;

        private Border wrapper = null!;

        private Transform RenderTransform
        {
            get
            {
                if (this.cachedRenderTransform == null || !object.ReferenceEquals(cachedRenderTransform, this.wrapper.RenderTransform))
                {
                    Transform clonedTransform = this.wrapper.RenderTransform.CloneCurrentValue();
                    cachedRenderTransform = clonedTransform;
                }
                return cachedRenderTransform;
            }
            set
            {
                if (this.cachedRenderTransform != value)
                {
                    this.cachedRenderTransform = value;
                    this.wrapper.RenderTransform = value;
                }
            }
        }

        public event EventHandler? MouseEventFinished;

        private MatrixTransform MyMatrixTransform
        {
            get
            {
                return (MatrixTransform)this.RenderTransform;
            }
        }

        /// <summary>
        /// 获取控件位置（相对父容器）
        /// </summary>
        public Point Position
        {
            get { return (Point)GetValue(PositionProperty); }
            set 
            { 
                SetValue(PositionProperty, value);
                var matrix = (MatrixTransform.Identity as MatrixTransform)!.Matrix;
                matrix.Translate(Position.X, Position.Y);
                MyMatrixTransform.Matrix = matrix;
            }
        }

        public static readonly DependencyProperty PositionProperty =
            DependencyProperty.Register("Position", typeof(Point), typeof(TranslateZoomBehavior), new PropertyMetadata(default(Point)));

        /// <summary>
        /// 获取控件大小
        /// </summary>
        public Size Size
        {
            get { return (Size)GetValue(SizeProperty); }
            set 
            { 
                SetValue(SizeProperty, value); 
                this.wrapper.Width = value.Width + 6;
                this.wrapper.Height = value.Height + 6;
                this.AssociatedObject.Width = value.Width;
                this.AssociatedObject.Height = value.Height;
            }
        }

        // Using a DependencyProperty as the backing store for Size.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty SizeProperty =
            DependencyProperty.Register("Size", typeof(Size), typeof(TranslateZoomBehavior), new PropertyMetadata(default(Size)));

        internal void ApplyTranslateTransform(double x, double y)
        {
            Matrix matrix = this.MyMatrixTransform.Matrix;
            matrix.Translate(x, y);
            this.MyMatrixTransform.Matrix = matrix;
        }

        internal Border AddWrapperBorder()
        {
            var container = this.AssociatedObject.Parent as Panel ?? throw new InvalidCastException();
            container.Children.Remove(this.AssociatedObject);
            var border = CreateBorder();
            container.Children.Add(border);

            return border;
        }

        private void SetSize(Size size)
        {
            if (this.Size == size) return;
            SetValue(SizeProperty, size);
        }

        private void SetPosition(Point position)
        {
            if (this.Position == position) return;
            SetValue(PositionProperty, position);
        }

        private Border CreateBorder()
        {
            var border = new Border
            {
                Width = this.AssociatedObject.Width + 6,
                Height = this.AssociatedObject.Height + 6,
                Child = this.AssociatedObject,
                BorderBrush = new SolidColorBrush(Colors.Transparent),
                BorderThickness = new Thickness(3),
                CornerRadius = new CornerRadius(1),
                HorizontalAlignment = HorizontalAlignment.Left,
                VerticalAlignment = VerticalAlignment.Top,
            };
            return border;
        }

        private bool IsCursorInZoomRange(Point position)
        {
            if (position.X > wrapper.Width - 10 && position.Y > wrapper.Height - 10) return true;

            return false;
        }

        private void SetCursor(Point position)
        {
            if (IsCursorInZoomRange(position))
            {
                wrapper.Cursor = Cursors.SizeNWSE;
            }
            else
            {
                wrapper.Cursor = Cursors.Arrow;
            }
        }

        private void MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (wrapper.Cursor != Cursors.Arrow) return;
            this.AssociatedObject.CaptureMouse();
            this.AssociatedObject.MouseMove += this.AssociatedObject_MouseMove;
            this.AssociatedObject.LostMouseCapture += this.AssociatedObject_LostMouseCapture;
            e.Handled = true;
            this.lastMousePoint = e.GetPosition(this.wrapper);
            this.isDragging = true;
        }

        
        private void MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            if (wrapper.Cursor != Cursors.Arrow) return;
            this.AssociatedObject.ReleaseMouseCapture();
            e.Handled = true;
            this.MouseEventFinished?.Invoke(this.AssociatedObject, new EventArgs());
        }

        private void AssociatedObject_LostMouseCapture(object sender, MouseEventArgs e)
        {
            this.isDragging = false;
            this.AssociatedObject.MouseMove -= this.AssociatedObject_MouseMove;
            this.AssociatedObject.LostMouseCapture -= this.AssociatedObject_LostMouseCapture;
            this.lastMousePoint = default;

            var position = wrapper.TranslatePoint(new Point(0, 0), wrapper.Parent as FrameworkElement);
            this.SetPosition(position);
        }

        private void AssociatedObject_MouseMove(object sender, MouseEventArgs e)
        {
            if (this.isDragging && !this.isAdjustingTranslate)
            {
                this.isAdjustingTranslate = true;
                Point newPoint = e.GetPosition(wrapper);
                Vector delta = newPoint - this.lastMousePoint;

                this.ApplyTranslateTransform(delta.X, delta.Y);
                
                this.lastMousePoint = e.GetPosition(wrapper);
                this.isAdjustingTranslate = false;
            }
        }

        private void Wrapper_MouseEnter(object sender, MouseEventArgs e)
        {
            this.wrapper.BorderBrush.BeginAnimation(SolidColorBrush.ColorProperty, new ColorAnimation(Colors.DeepSkyBlue, new Duration(TimeSpan.FromMilliseconds(200))));
        }

        private void Wrapper_MouseLeave(object sender, MouseEventArgs e)
        {
            this.wrapper.BorderBrush.BeginAnimation(SolidColorBrush.ColorProperty, new ColorAnimation(Colors.Transparent, new Duration(TimeSpan.FromMilliseconds(200))));
        }

        private void Wrapper_MouseMove(object sender, MouseEventArgs e)
        {
            var pos = e.GetPosition(wrapper.Parent as IInputElement);
            this.SetCursor(e.GetPosition(wrapper));
            if (this.isAdjustingZoom)
            {
                var delta = pos - this.lastMousePoint;

                // 限制最小缩放
                if (delta.X < 0 && wrapper.Width < 15) delta.X = 0;
                if (delta.Y < 0 && wrapper.Height < 15) delta.Y = 0;

                wrapper.Width += delta.X;
                wrapper.Height += delta.Y;
                this.AssociatedObject.Width = wrapper.Width - 6;
                this.AssociatedObject.Height = wrapper.Height - 6;
                this.lastMousePoint = e.GetPosition(wrapper.Parent as IInputElement);
            }
        }

        private void Wrapper_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            if (!IsCursorInZoomRange(e.GetPosition(wrapper))) return;
            wrapper.CaptureMouse();
            this.isAdjustingZoom = true;
            lastMousePoint = e.GetPosition(wrapper.Parent as IInputElement);
        }

        private void Wrapper_MouseLeftButtonUp(object sender, MouseButtonEventArgs e)
        {
            wrapper.ReleaseMouseCapture();
            this.isAdjustingZoom = false;
            lastMousePoint = default;
            this.SetSize(new Size(this.AssociatedObject.Width, this.AssociatedObject.Height));
            this.MouseEventFinished?.Invoke(this, new());
        }

        protected override void OnAttached()
        {
            this.wrapper = AddWrapperBorder();
            
            this.SetSize(new Size(wrapper.Width, wrapper.Height));

            this.RenderTransform = new MatrixTransform(wrapper.RenderTransform.Value);
            
            this.AssociatedObject.AddHandler(UIElement.MouseLeftButtonDownEvent, new MouseButtonEventHandler(this.MouseLeftButtonDown), false);
            this.AssociatedObject.AddHandler(UIElement.MouseLeftButtonUpEvent, new MouseButtonEventHandler(this.MouseLeftButtonUp), false);

            this.wrapper.AddHandler(UIElement.MouseEnterEvent, new MouseEventHandler(this.Wrapper_MouseEnter), false);
            this.wrapper.AddHandler(UIElement.MouseLeaveEvent, new MouseEventHandler(this.Wrapper_MouseLeave), false);
            this.wrapper.AddHandler(UIElement.MouseMoveEvent, new MouseEventHandler(this.Wrapper_MouseMove), false);
            this.wrapper.AddHandler(UIElement.MouseLeftButtonDownEvent, new MouseButtonEventHandler(this.Wrapper_MouseLeftButtonDown), false);
            this.wrapper.AddHandler(UIElement.MouseLeftButtonUpEvent, new MouseButtonEventHandler(this.Wrapper_MouseLeftButtonUp), false);
        }

        protected override void OnDetaching()
        {
            this.wrapper.RemoveHandler(UIElement.MouseEnterEvent, new MouseEventHandler(this.Wrapper_MouseEnter));
            this.wrapper.RemoveHandler(UIElement.MouseLeaveEvent, new MouseEventHandler(this.Wrapper_MouseLeave));
            this.wrapper.RemoveHandler(UIElement.MouseMoveEvent, new MouseEventHandler(this.Wrapper_MouseMove));
            this.wrapper.RemoveHandler(UIElement.MouseLeftButtonDownEvent, new MouseButtonEventHandler(this.Wrapper_MouseLeftButtonDown));
            this.wrapper.RemoveHandler(UIElement.MouseLeftButtonUpEvent, new MouseButtonEventHandler(this.Wrapper_MouseLeftButtonUp));

            this.AssociatedObject.RemoveHandler(UIElement.MouseLeftButtonDownEvent, new MouseButtonEventHandler(this.MouseLeftButtonDown));
            this.AssociatedObject.RemoveHandler(UIElement.MouseLeftButtonUpEvent, new MouseButtonEventHandler(this.MouseLeftButtonUp));

            this.wrapper.Child = null;

            var parent = this.wrapper.Parent as Panel;
            parent!.Children.Remove(this.wrapper);
        }
    }
}
