﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace SimpleBoard.Trigger
{
    public class StringFilter : Filter
    {
        public string RegexExpression { get; set; } = null!;

        public override bool IsFitCondition(object propertyValue)
        {
            var str = propertyValue as string;
            if (str is null) return false;
            return Regex.IsMatch(str, RegexExpression);
        }
    }
}
