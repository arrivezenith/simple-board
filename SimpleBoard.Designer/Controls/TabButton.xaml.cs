﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SimpleBoard.Designer.Controls
{
    /// <summary>
    /// TabButton.xaml 的交互逻辑
    /// </summary>
    public partial class TabButton : UserControl
    {
        public TabButton()
        {
            InitializeComponent();
            DataContext = this;
        }

        public bool IsActive
        {
            get { return (bool)GetValue(IsActiveProperty); }
            set { SetValue(IsActiveProperty, value); }
        }


        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Title.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TitleProperty =
            DependencyProperty.Register("Title", typeof(string), typeof(TabButton), new PropertyMetadata("新建标签页"));



        private static readonly Brush activeBackground = new SolidColorBrush(Color.FromRgb(0, 108, 190));
        private static readonly Brush inActiveBackground = new SolidColorBrush(Color.FromRgb(226, 226, 226));

        // Using a DependencyProperty as the backing store for IsActive.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IsActiveProperty =
            DependencyProperty.Register("IsActive", typeof(bool), typeof(TabButton), new PropertyMetadata(false, (s, e) =>
            {
                var tabButton = s as TabButton;
                var active = (bool)e.NewValue;
                tabButton!.Foreground = active? Brushes.White : Brushes.Black;
                tabButton!.Background = active ? activeBackground : inActiveBackground;
                tabButton!.CloseButton.Visibility = active ? Visibility.Visible : Visibility.Collapsed;
            }));

        private void Grid_MouseEnter(object sender, MouseEventArgs e)
        {
            Foreground = Brushes.White;
            Background = activeBackground;
            CloseButton.Visibility = Visibility.Visible;
        }

        private void Grid_MouseLeave(object sender, MouseEventArgs e)
        {
            if (IsActive) return;
            Foreground = Brushes.Black;
            Background = inActiveBackground;
            CloseButton.Visibility = Visibility.Collapsed;
        }
    }
}
