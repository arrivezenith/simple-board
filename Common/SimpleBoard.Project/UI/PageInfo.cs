﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBoard.Project.UI
{
    public class PageInfo
    {
        public PageInfo()
        {
            Controls = new List<ControlInfo>();
        }

        public int Width { get; set; }
        public int Height { get; set; }
        public string BackgroundColor { get; set; } = null!;
        public ICollection<ControlInfo> Controls { get; set; } = null!;
    }
}
