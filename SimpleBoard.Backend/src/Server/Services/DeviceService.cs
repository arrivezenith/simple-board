﻿using SimpleBoard.Backend.Server.Models;
using System.Collections;
using System.Collections.Generic;
using System.Text.Json;

namespace SimpleBoard.Backend.Server.Services
{
    public interface IDeviceService
    {
        DataFrameMeta ParseDataProtocol(string json);
    }

    public class DeviceService : IDeviceService
    {
        public DataFrameMeta ParseDataProtocol(string json)
        {
            return JsonSerializer.Deserialize<DataFrameMeta>(json)!;
        }
    }
}
