﻿using DSF.Net.Controllers.Theme;
using SimpleBoard.ControllerMeta;
using System;
using System.CodeDom;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DSF.Net.Controllers.Process
{
    /// <summary>
    /// RoundLabel.xaml 的交互逻辑
    /// </summary>
    [SimpleBoardController("DSF指示器-圆形百分比")]
    [HasConfigurableProperty(nameof(Value), PropertyType.Double, "数值")]
    public partial class RoundLabel : UserControl,IUserTheme
    {
        public RoundLabel()
        {
            InitializeComponent();
            DataContext = this;
            Width = 56;
            Height = 56;
        }



        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Value.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(double), typeof(RoundLabel), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as RoundLabel;
                var oldValue = (double)e.OldValue;
                var newValue = (double)e.NewValue;
                var animation = new DoubleAnimation
                {
                    From = oldValue,
                    To = newValue,
                    Duration = new Duration(new TimeSpan(0, 0, 0, 0, 500)),
                    EasingFunction = new QuadraticEase()
                };
                c.BeginAnimation(ArcAnimateProperty, animation);
                c.BeginAnimation(TextAnimateProperty, animation);
            }));

        private static readonly DependencyProperty ArcAnimateProperty =
            DependencyProperty.Register("ArcAnimate", typeof(double), typeof(RoundLabel), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as RoundLabel;
                var value = (double)e.NewValue;
                c.arc.EndAngle = value / 100 * 360;
            }));
        private static readonly DependencyProperty TextAnimateProperty =
            DependencyProperty.Register("TextAnimate", typeof(double), typeof(RoundLabel), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as RoundLabel;
                var value = (double)e.NewValue;
                c.label.Content = value.ToString("#0.0") + '%';
            }));
        public Theme.Colors ThemeColor
        {
            get { return (Theme.Colors)GetValue(ThemeColorProperty); }
            set { SetValue(ThemeColorProperty, value); }
        }

        public static readonly DependencyProperty ThemeColorProperty =
            DependencyProperty.Register("ThemeColor", typeof(Theme.Colors), typeof(RoundLabel), new PropertyMetadata(Theme.Colors.Default));

    }
}
