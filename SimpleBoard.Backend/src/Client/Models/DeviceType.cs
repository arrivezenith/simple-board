﻿using System.Collections.Generic;
using System.Linq;

namespace SimpleBoard.Backend.Client.Models
{
    public class DeviceType
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DataFrameMeta Protocol { get; set; }
    }

    public class DataFrameMeta
    {
        public IEnumerable<DataUnit> DataUnits { get; set; } = null!;

        public DataUnit this[int index]
        {
            get => DataUnits.Skip(index).First();
        }
    }

    public enum DataUnitType
    {
        Int32,
        Float32,
        String,
        Bool
    }

    public class DataUnit
    {
        public string Name { get; set; } = null!;
        public DataUnitType Type { get; set; }
    }
}
