﻿namespace SimpleBoard.Backend.Client.Models
{
    public class ApiResult<T>
    {
        public int Code { get; set; }
        public string Message { get; set; } = null!;
        public T Data { get; set; }
    }
}
