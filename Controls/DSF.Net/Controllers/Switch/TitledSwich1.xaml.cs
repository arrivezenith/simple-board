﻿using DSF.Net.Controllers.Theme;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DSF.Net.Controllers.Switch
{
    /// <summary>
    /// TitledSwich1.xaml 的交互逻辑
    /// </summary>
    public partial class TitledSwich1 : UserControl, IUserTheme
    {
        /// <summary>
        /// 当控件状态值发生变化时调用该事件
        /// </summary>
        public event EventHandler ValueChanged;

        public TitledSwich1()
        {
            InitializeComponent();
            DataContext = this;
            Touch.MouseLeftButtonDown += Touch_MouseLeftButtonDown;
        }

        private void Touch_MouseLeftButtonDown(object sender, MouseButtonEventArgs e)
        {
            Value = Value == SwitchStatus.Off ? SwitchStatus.On : SwitchStatus.Off;
            ValueChanged?.Invoke(this, null);
        }

        /// <summary>
        /// 获取/设置控件上显示的文本
        /// </summary>
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        /// <summary>
        /// 获取/设置状态为On时的文本
        /// </summary>
        public string TextOn
        {
            get { return (string)GetValue(TextOnProperty); }
            set { SetValue(TextOnProperty, value); }
        }

        /// <summary>
        /// 获取/设置状态为Off时的文本
        /// </summary>
        public string TextOff
        {
            get { return (string)GetValue(TextOffProperty); }
            set { SetValue(TextOffProperty, value); }
        }

        /// <summary>
        /// 获取/设置控件的状态值
        /// </summary>
        public SwitchStatus Value
        {
            get { return (SwitchStatus)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public Theme.Colors ThemeColor
        {
            get { return (Theme.Colors)GetValue(ThemeColorProperty); }
            set { SetValue(ThemeColorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ThemeColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ThemeColorProperty =
            DependencyProperty.Register("ThemeColor", typeof(Theme.Colors), typeof(TitledSwich1), new PropertyMetadata(Theme.Colors.Default));



        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(SwitchStatus), typeof(TitledSwich1), new PropertyMetadata(SwitchStatus.On, (s, e) =>
            {
                var c = s as TitledSwich1;
                var value = (SwitchStatus)e.NewValue;
                c.SwitchOn.Visibility = value == SwitchStatus.On ? Visibility.Visible : Visibility.Hidden;
                c.SwitchOff.Visibility = value == SwitchStatus.Off ? Visibility.Visible : Visibility.Hidden;
            }));
        public static readonly DependencyProperty TextOnProperty =
            DependencyProperty.Register("TextOn", typeof(string), typeof(TitledSwich1), new PropertyMetadata("On", (s, e) =>
            {
                var c = s as TitledSwich1;
                c.LabelOn.Content = e.NewValue;
            }));
        public static readonly DependencyProperty TextOffProperty =
            DependencyProperty.Register("TextOff", typeof(string), typeof(TitledSwich1), new PropertyMetadata("Off", (s, e) =>
            {
                var c = s as TitledSwich1;
                c.LabelOff.Content = e.NewValue;
            }));
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(TitledSwich1), new PropertyMetadata("TitledSwitch", (s, e) =>
            {
                var c = s as TitledSwich1;
                c.TitleLabel.Content = e.NewValue;
            }));

    }
}
