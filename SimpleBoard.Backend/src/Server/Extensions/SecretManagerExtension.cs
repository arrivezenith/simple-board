﻿using Microsoft.Extensions.DependencyInjection;
using SimpleBoard.Backend.Server.Configurations;
using SimpleBoard.Backend.Server.Entities;
using SimpleBoard.Backend.Server.Services.Auth;
using System;

namespace SimpleBoard.Backend.Server.Extensions
{
    public static class SecretManagerExtension
    {
        public static IServiceCollection AddUserSecretManager(this IServiceCollection services, Action<SecretManagerConfiguration> options)
        {
            var config = new SecretManagerConfiguration();
            options(config);

            if (config.SymmetricKey is null) throw new Exception(nameof(config.SymmetricKey));
            if (config.HashSalt is null) throw new Exception(nameof(config.HashSalt));
            if (config.SymmetricSalt is null) throw new Exception(nameof(config.SymmetricSalt));
            services.AddTransient<ISecretManager, UserSecretManager>(services =>
            {
                var dbContext = services.GetRequiredService<ApplicationDbContext>();
                return new UserSecretManager(dbContext, config);
            });

            return services;
        }
    }
}
