﻿using SimpleBoard.DependencyInjection;
using SimpleBoard.Designer.Controls;
using SimpleBoard.Designer.Extensions;
using SimpleBoard.Designer.Pages;
using SimpleBoard.Designer.Services;
using SimpleBoard.Designer.ViewModels;
using SimpleBoard.Project.Binding;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SimpleBoard.Designer
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    [InjectAs(InjectionType.Singleton)]
    public partial class MainWindow : Window
    {
        public MainWindow(ControllerService controllerService)
        {
            InitializeComponent();
            this.controllerService = controllerService;
            this.MainPage.Navigate(App.AutoWired<WelcomePage>());

            this.previousTab = WelcomeTab;
        }

        private readonly ControllerService controllerService;

        private TabButton? previousTab;

        private void OnFileTabClicked(object sender, MouseButtonEventArgs e)
        {
            var button = sender as TabButton;
            if (ReferenceEquals(button, previousTab)) return;
            if (previousTab is not null) previousTab.IsActive = false;
            button!.IsActive = true;
            previousTab = button;

            Page page = (string)button.Tag switch
            {
                "Welcome" => App.AutoWired<WelcomePage>(),
                "Designer" => App.AutoWired<DesignerPage>(),
                "Protocol" => App.AutoWired<DataProtocolDesignerPage>(),
                _ => App.AutoWired<WelcomePage>(),
            };

            this.MainPage.Navigate(page);
        }

        private void OnLeftSideTabClicked(object sender, MouseButtonEventArgs e)
        {
            var button = sender as SideTabButton;
            if (button is null) return; 
            if (LeftPanel.Visibility == Visibility.Visible && (string)LeftPanelTitle.Content == button.Title)
            {
                LeftPanel.Visibility = Visibility.Collapsed;
                return;
            }
            LeftPanelTitle.Content = button.Title;
            Page page = button.Title switch
            {
                "工具箱" => App.AutoWired<ToolboxPanel>(),
                "属性" => App.AutoWired<PropertyPanel>(),
                _ => App.AutoWired<WelcomePage>()
            };

            LeftPanelFrame.Navigate(page);
            LeftPanel.Visibility = Visibility.Visible;
        }

        private void OnRightSideTabClicked(object sender, MouseButtonEventArgs e)
        {
            var button = sender as SideTabButton;
            if (button is null) return;
            if (RightPanel.Visibility == Visibility.Visible && (string)RightPanelTitle.Content == button.Title)
            {
                RightPanel.Visibility = Visibility.Collapsed;
                return;
            }
            RightPanelTitle.Content = button.Title;
            Page page = button.Title switch
            {
                "数据源" => App.AutoWired<DataSourcePanel>(),
                _ => App.AutoWired<WelcomePage>()
            };

            RightPanelFrame.Navigate(page);
            RightPanel.Visibility = Visibility.Visible;
        }

        private void MenuItem_Click(object sender, RoutedEventArgs e)
        {
            var menuItem = sender as MenuItem;
            switch (menuItem!.Header)
            {
                case "加载":
                    var savedProject = App.AutoWired<ProjectService>().LoadProject();
                    if (savedProject == null) return;
                    App.AutoWired<DesignerPage>().LoadPage(savedProject.Page);
                    var dataDesignVm = App.AutoWired<DataProtocolDesignerPageViewModel>();
                    dataDesignVm.DataUnits.Clear();
                    dataDesignVm.DataBindings.Clear();
                    foreach(var dataUnit in savedProject.DataFrameMeta.DataUnits)
                    {
                        dataDesignVm.DataUnits.Add(dataUnit);
                    }
                    foreach(var databinding in savedProject.DataBindings ?? Enumerable.Empty<DataBinding>())
                    {
                        dataDesignVm.DataBindings.Add(databinding);
                    }
                    break;
                case "保存":
                    var uiRoot = App.AutoWired<DesignerPage>().Container;
                    dataDesignVm = App.AutoWired<DataProtocolDesignerPageViewModel>();
                    var project = App.AutoWired<ProjectService>().ParseProject(uiRoot, dataDesignVm.DataUnits, dataDesignVm.DataBindings);
                    App.AutoWired<ProjectService>().SaveProject(project);
                    break;
                case "编译":
                    uiRoot = App.AutoWired<DesignerPage>().Container;
                    dataDesignVm = App.AutoWired<DataProtocolDesignerPageViewModel>();
                    project = App.AutoWired<ProjectService>().ParseProject(uiRoot, dataDesignVm.DataUnits, dataDesignVm.DataBindings);
                    var result = App.AutoWired<ProjectService>().CompileProject(project);
                    System.IO.File.Move(result.PageGeneratorAssemblyPath, "App/App.dll", true);
                    System.IO.File.Move(result.DataProtocolJsonPath, "App/Protocol.json", true);
                    break;
                case "运行":
                    Process.Start("SimpleBoard.Runtime.exe");
                    break;
            }
        }



        //private void Button_Click(object sender, RoutedEventArgs e)
        //{
        //    var labelInfo = controllerService.Controllers.First();

        //    var label = this.root.AddController(labelInfo);
        //    label.Width = 200;
        //    label.Height = 200;

        //    label.SetPropertyValue("Content", "Test");
        //    label.SetPropertyValue("Background", Brushes.Gold);

        //    label.AddDragZoomBehavior();
        //}
    }
}
