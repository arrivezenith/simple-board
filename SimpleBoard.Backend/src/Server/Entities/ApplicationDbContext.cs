﻿using Microsoft.EntityFrameworkCore;
using SimpleBoard.Backend.Server.Entities.Users;

namespace SimpleBoard.Backend.Server.Entities
{
    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions options) 
            : base(options)
        {

        }

        /// <summary>
        /// 设备表
        /// </summary>
        public DbSet<Device> Devices { get; set; } = null!;

        /// <summary>
        /// 设备注册表
        /// </summary>
        public DbSet<DeviceRestry> DeviceRestries { get; set; } = null!;

        /// <summary>
        /// 员工表
        /// </summary>
        public DbSet<Employee> Employee { get; set; } = null!;

        /// <summary>
        /// 权限声明表
        /// </summary>
        public DbSet<UserClaim> UserClaims { get; set; } = null!;

        /// <summary>
        /// 用户密钥表
        /// </summary>
        public DbSet<UserSecret> UserSecrets { get; set; } = null!;
    }
}
