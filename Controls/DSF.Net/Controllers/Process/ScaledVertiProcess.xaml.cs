﻿using DSF.Net.Controllers.Theme;
using SimpleBoard.ControllerMeta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DSF.Net.Controllers.Process
{
    /// <summary>
    /// ScaledVertiProcess.xaml 的交互逻辑
    /// </summary>
    [SimpleBoardController("DSF指示器-竖直单区间文本")]
    [HasConfigurableProperty(nameof(Value), PropertyType.Double, "数值")]
    [HasConfigurableProperty(nameof(Limit), PropertyType.Double, "区间长度")]
    public partial class ScaledVertiProcess : UserControl,IUserTheme
    {
        public ScaledVertiProcess()
        {
            InitializeComponent();
            DataContext = this;
            Width = 110;
            Height = 240;
        }

        int PointerRange = 194;
        int PointerStart = 6;
        int GridValueMax = 196;

        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Value.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(double), typeof(ScaledVertiProcess), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as ScaledVertiProcess;
                var oldValue = (double)e.OldValue;
                var newValue = (double)e.NewValue;
                var animation = new DoubleAnimation
                {
                    From = oldValue,
                    To = newValue,
                    Duration = new Duration(new TimeSpan(0, 0, 0, 0, 500)),
                    EasingFunction = new QuadraticEase()
                };
                c.BeginAnimation(AnimateProperty, animation);
            }));


        public double Limit
        {
            get { return (double)GetValue(LimitProperty); }
            set { SetValue(LimitProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Limit.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LimitProperty =
            DependencyProperty.Register("Limit", typeof(double), typeof(ScaledVertiProcess), new PropertyMetadata(100d, (s, e) =>
            {
                var c = s as ScaledVertiProcess;
                var limit = (double)e.NewValue;
                c.gridValue.Height = c.Value / limit * c.GridValueMax;
                c.gridPointer.Margin = new Thickness(c.gridPointer.Margin.Left, c.gridPointer.Margin.Top,
                    c.gridPointer.Margin.Right, c.PointerStart + c.Value / limit * c.PointerRange);
                c.lableLimit.Content = $"{limit}{c.Unit}";
            }));

        public string Unit
        {
            get { return (string)GetValue(UnitProperty); }
            set { SetValue(UnitProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Unit.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty UnitProperty =
            DependencyProperty.Register("Unit", typeof(string), typeof(ScaledVertiProcess), new PropertyMetadata("V", (s, e) =>
            {
                var c = s as ScaledVertiProcess;
                var unit = (string)e.NewValue;
                c.lableLimit.Content = $"{c.Value}{unit}";
                c.labelValue.Content = ((string)c.labelValue.Content).Replace((string)e.OldValue, "") + unit;
            }));


        public int Precision
        {
            get { return (int)GetValue(PrecisionProperty); }
            set { SetValue(PrecisionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Precision.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PrecisionProperty =
            DependencyProperty.Register("Precision", typeof(int), typeof(ScaledVertiProcess), new PropertyMetadata(2, (s, e) =>
            {
                var c = s as ScaledVertiProcess;
                var value = (int)e.NewValue;
                var priciseStr = new StringBuilder("#0.");
                for (var i = 0; i < value; i++)
                {
                    priciseStr.Append('0');
                }
                c.labelValue.Content = c.Value.ToString(priciseStr.ToString()) + c.Unit;
            }));

        private static readonly DependencyProperty AnimateProperty =
            DependencyProperty.Register("Animate", typeof(double), typeof(ScaledVertiProcess), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as ScaledVertiProcess;
                var value = (double)e.NewValue;
                c.gridValue.Height = value / c.Limit * c.GridValueMax;
                c.gridPointer.Margin = new Thickness(c.gridPointer.Margin.Left, c.gridPointer.Margin.Top,
                    c.gridPointer.Margin.Right, c.PointerStart + value / c.Limit * c.PointerRange);
                var priciseStr = new StringBuilder("#0.");
                for (var i = 0; i < c.Precision; i++)
                {
                    priciseStr.Append('0');
                }
                c.labelValue.Content = value.ToString(priciseStr.ToString()) + c.Unit;
            }));


        public Theme.Colors ThemeColor
        {
            get { return (Theme.Colors)GetValue(ThemeColorProperty); }
            set { SetValue(ThemeColorProperty, value); }
        }

        public static readonly DependencyProperty ThemeColorProperty =
            DependencyProperty.Register("ThemeColor", typeof(Theme.Colors), typeof(ScaledVertiProcess), new PropertyMetadata(Theme.Colors.Default));

    }
}
