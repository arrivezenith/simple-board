﻿using DSF.Net.Controllers.Theme;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DSF.Net.Controllers.Label
{
    /// <summary>
    /// AlertIndicator.xaml 的交互逻辑
    /// </summary>
    public partial class AlertIndicator : UserControl,IUserTheme
    {
        readonly double downlimit = 0;
        readonly double width = 16;

        public AlertIndicator()
        {
            InitializeComponent();
            DataContext = this;
        }

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(AlertIndicator), new PropertyMetadata("ALERT", (s, e) =>
            {
                var c = s as AlertIndicator;
                c.labelText.Content = e.NewValue;
            }));



        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }


        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(double), typeof(AlertIndicator), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as AlertIndicator;
                var oldValue = (double)e.OldValue;
                var newValue = (double)e.NewValue;
                var animation = new DoubleAnimation
                {
                    From = oldValue,
                    To = newValue,
                    Duration = new Duration(new TimeSpan(0, 0, 0, 0, 500)),
                    EasingFunction = new QuadraticEase()
                };
                c.Value = (c.Value >= c.downlimit) ? c.Value : c.downlimit;
                c.Value = (c.Value <= c.Limit) ? c.Value : c.Limit;
                c.Limit = (c.Limit >= c.Value) ? c.Limit : c.Value;

                c.BeginAnimation(AnimateProperty, animation);
                
                c.AlertGrid.Width = (double)(c.Value / c.Limit * c.width);
            }));

        public double Limit
        {
            get { return (double)GetValue(LimitProperty); }
            set { SetValue(LimitProperty, value); }
        }

       
        // Using a DependencyProperty as the backing store for Limit.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LimitProperty =
            DependencyProperty.Register("Limit", typeof(double), typeof(AlertIndicator), new PropertyMetadata(3d, (s, e) =>
            {
                var c = s as AlertIndicator;
                var oldValue = (double)e.OldValue;
                var newValue = (double)e.NewValue;
                var animation = new DoubleAnimation
                {
                    From = oldValue,
                    To = newValue,
                    Duration = new Duration(new TimeSpan(0, 0, 0, 0, 500)),
                    EasingFunction = new QuadraticEase()
                };
                c.Value = (c.Value >= c.downlimit) ? c.Value : c.downlimit;
                c.Value = (c.Value <= c.Limit) ? c.Value : c.Limit;
                c.Limit = (c.Limit >= c.Value) ? c.Limit : c.Value;

                c.BeginAnimation(AnimateProperty, animation);
                c.AlertGrid.Width = c.Value / (double)newValue * c.width;
            }));

        private static readonly DependencyProperty AnimateProperty =
            DependencyProperty.Register("Animate", typeof(double), typeof(AlertIndicator), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as AlertIndicator;
                var value = (double)e.NewValue;
                c.Value = (c.Value >= c.downlimit) ? c.Value : c.downlimit;
                c.Value = (c.Value <= c.Limit) ? c.Value : c.Limit;
                c.Limit = (c.Limit >= c.Value) ? c.Limit : c.Value;

                c.AlertGrid.Width = c.Value / (double)c.Limit * c.width;

            }));

        public Theme.Colors ThemeColor
        {
            get { return (Theme.Colors)GetValue(ThemeColorProperty); }
            set { SetValue(ThemeColorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ThemeColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ThemeColorProperty =
            DependencyProperty.Register("ThemeColor", typeof(Theme.Colors), typeof(AlertIndicator), new PropertyMetadata(Theme.Colors.Default));

    }
}
