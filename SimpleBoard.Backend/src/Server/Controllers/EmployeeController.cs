﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SimpleBoard.Backend.Server.Entities;
using SimpleBoard.Backend.Server.Extensions;
using SimpleBoard.Backend.Server.Services.Auth;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleBoard.Backend.Server.Controllers
{
    // TODO 鉴权和日志
    [Route("api/[controller]")]
    [ApiController]
    public class EmployeeController : ControllerBase
    {
        public EmployeeController(ApplicationDbContext dbContext, ILogger<DeviceController> logger, ISecretManager secretManager)
        {
            this.dbContext = dbContext;
            this.logger = logger;
            this.secretManager = secretManager;
        }

        private readonly ApplicationDbContext dbContext;
        private readonly ILogger logger;
        private readonly ISecretManager secretManager;

        /// <summary>
        /// 添加员工
        /// </summary>
        /// <param name="employeeId">工号</param>
        /// <param name="name">姓名</param>
        /// <param name="email">邮箱</param>
        /// <param name="phone">手机号</param>
        /// <param name="password">密码</param>
        /// <returns></returns>
        [HttpPost("[Action]")]
        public async Task<IActionResult> AddEmployee([FromForm]int employeeId, [FromForm]string name, 
            [FromForm]string email, [FromForm]string phone, [FromForm]string password)
        {
            var exsistsQuery = from user in dbContext.Employee
                               where user.EmployeeId == employeeId
                               select user.Id;
            if (exsistsQuery.Any())
            {
                return this.Fail("指定工号的员工已存在");
            }

            var newUser = dbContext.Employee.Add(new Entities.Users.Employee
            {
                Name = name,
                Email = email,
                Contact = phone,
                EmployeeId = employeeId,
            }).Entity;

            await dbContext.SaveChangesAsync();

            secretManager.Bind(newUser.Id);
            secretManager.StorageHashSecret("password", password);

            return this.Success("添加员工成功");
        }

        /// <summary>
        /// 删除员工
        /// </summary>
        /// <param name="employeeId">工号</param>
        /// <returns></returns>
        [HttpGet("[Action]")]
        public async Task<IActionResult> RemoveEmployee(int employeeId)
        {
            var exsistsQuery = from user in dbContext.Employee
                               where user.EmployeeId == employeeId
                               select user;
            if (!exsistsQuery.Any())
            {
                return this.Fail("指定工号的员工不存在");
            }

            dbContext.Employee.Remove(exsistsQuery.Single());
            await dbContext.SaveChangesAsync();

            return this.Success("删除员工成功");
        }

        /// <summary>
        /// 获取员工列表
        /// </summary>
        /// <returns></returns>
        [HttpGet("[Action]")]
        public async Task<IActionResult> GetEmployeeList()
        {
            return this.Success("获取成功", await dbContext.Employee.Select(x => new
            {
                x.EmployeeId,
                x.Name,
                x.Email,
                x.Contact
            }).ToListAsync());
        }
    }
}
