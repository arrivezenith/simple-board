﻿using DSF.Net.Controllers.Theme;
using SimpleBoard.ControllerMeta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DSF.Net.Controllers.Process
{
    /// <summary>
    /// ScaledHoriProcess.xaml 的交互逻辑
    /// </summary>
    [SimpleBoardController("DSF指示器-水平百分比")]
    [HasConfigurableProperty(nameof(Value), PropertyType.Double, "数值")]
    [HasConfigurableProperty(nameof(Text), PropertyType.String, "文本")]
    [HasConfigurableProperty(nameof(Precision), PropertyType.Int32, "保留小数")]
    public partial class ScaledHoriProcess : UserControl,IUserTheme
    {
        public ScaledHoriProcess()
        {
            InitializeComponent();
            DataContext = this;
            Width = 160;
            Height = 32;
        }



        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(ScaledHoriProcess), new PropertyMetadata("Process", (s, e) =>
            {
                var c = s as ScaledHoriProcess;
                c.labelText.Content = e.NewValue;
            }));

        public string Unit
        {
            get { return (string)GetValue(UnitProperty); }
            set { SetValue(UnitProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Unit.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty UnitProperty =
            DependencyProperty.Register("Unit", typeof(string), typeof(ScaledHoriProcess), new PropertyMetadata("%", (s, e) =>
            {
                var c = s as ScaledHoriProcess;
                c.labelUnit.Content = e.NewValue;
            }));

        public double Limit
        {
            get { return (double)GetValue(LimitProperty); }
            set { SetValue(LimitProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Limit.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LimitProperty =
            DependencyProperty.Register("Limit", typeof(double), typeof(ScaledHoriProcess), new PropertyMetadata(100d, (s, e) =>
            {
                var c = s as ScaledHoriProcess;
                var value = (double)e.NewValue;
                c.bar.Width = 96 * c.Value / value;
            }));

        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Value.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(double), typeof(ScaledHoriProcess), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as ScaledHoriProcess;
                var oldValue = (double)e.OldValue;
                var newValue = (double)e.NewValue;
                var animation = new DoubleAnimation
                {
                    From = oldValue,
                    To = newValue,
                    Duration = new Duration(new TimeSpan(0, 0, 0, 0, 500)),
                    EasingFunction = new QuadraticEase()
                };
                c.BeginAnimation(TextAnimateProperty, animation);
                c.BeginAnimation(BarAnimateProperty, animation);
            }));

        public int Precision
        {
            get { return (int)GetValue(PrecisionProperty); }
            set { SetValue(PrecisionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Precision.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PrecisionProperty =
            DependencyProperty.Register("Precision", typeof(int), typeof(ScaledHoriProcess), new PropertyMetadata(2, (s, e) => {
                var c = s as ScaledHoriProcess;
                var value = (int)e.NewValue;
                var priciseStr = new StringBuilder("#0.");
                for (var i = 0; i < value; i++)
                {
                    priciseStr.Append('0');
                }
                c.labelValue.Content = c.Value.ToString(priciseStr.ToString());
            }));

        private static readonly DependencyProperty TextAnimateProperty =
            DependencyProperty.Register("TextAnimate", typeof(double), typeof(ScaledHoriProcess), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as ScaledHoriProcess;
                var value = (double)e.NewValue;
                var priciseStr = new StringBuilder("#0.");
                for (var i = 0; i < c.Precision; i++)
                {
                    priciseStr.Append('0');
                }
                c.labelValue.Content = value.ToString(priciseStr.ToString());
            }));

        private static readonly DependencyProperty BarAnimateProperty =
            DependencyProperty.Register("BarAnimate", typeof(double), typeof(ScaledHoriProcess), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as ScaledHoriProcess;
                var value = (double)e.NewValue;
                c.bar.Width = 96 * value / c.Limit;
            }));


        public Theme.Colors ThemeColor
        {
            get { return (Theme.Colors)GetValue(ThemeColorProperty); }
            set { SetValue(ThemeColorProperty, value); }
        }

        public static readonly DependencyProperty ThemeColorProperty =
            DependencyProperty.Register("ThemeColor", typeof(Theme.Colors), typeof(ScaledHoriProcess), new PropertyMetadata(Theme.Colors.Default));

    }
}
