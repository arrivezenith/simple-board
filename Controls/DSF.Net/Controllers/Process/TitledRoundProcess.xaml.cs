﻿using DSF.Net.Controllers.Theme;
using SimpleBoard.ControllerMeta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DSF.Net.Controllers.Process
{
    /// <summary>
    /// TitledRoundProcess.xaml 的交互逻辑
    /// </summary>
    [SimpleBoardController("DSF指示器-圆形单区间文本")]
    [HasConfigurableProperty(nameof(Value), PropertyType.Double, "数值")]
    [HasConfigurableProperty(nameof(Text), PropertyType.String, "文本")]
    [HasConfigurableProperty(nameof(Precision), PropertyType.Int32, "保留小数")]
    public partial class TitledRoundProcess : UserControl,IUserTheme
    {
        public TitledRoundProcess()
        {
            InitializeComponent();
            DataContext = this;
            Width = 140;
            Height = 140;
        }
        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(TitledRoundProcess), new PropertyMetadata("Height/m", (s, e) =>
            {
                var c = s as TitledRoundProcess;
                c.labelText.Content = e.NewValue;
            }));

        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Value.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(double), typeof(TitledRoundProcess), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as TitledRoundProcess;
                var oldValue = (double)e.OldValue;
                var newValue = (double)e.NewValue;
                var animation = new DoubleAnimation
                {
                    From = oldValue,
                    To = newValue,
                    Duration = new Duration(new TimeSpan(0, 0, 0, 0, 500)),
                    EasingFunction = new QuadraticEase()
                };
                c.BeginAnimation(AnimateProperty, animation);
            }));

        public double Limit
        {
            get { return (double)GetValue(LimitProperty); }
            set { SetValue(LimitProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Limit.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LimitProperty =
            DependencyProperty.Register("Limit", typeof(double), typeof(TitledRoundProcess), new PropertyMetadata(100d, (s, e) =>
            {
                var c = s as TitledRoundProcess;
                c.arc.EndAngle = c.Value / (double)e.NewValue * 360;
            }));

        public int Precision
        {
            get { return (int)GetValue(PrecisionProperty); }
            set { SetValue(PrecisionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Precision.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PrecisionProperty =
            DependencyProperty.Register("Precision", typeof(int), typeof(TitledRoundProcess), new PropertyMetadata(3, (s, e) =>
            {
                var c = s as TitledRoundProcess;
                var precision = (int)e.NewValue;
                var precisionStr = new StringBuilder("#0.");
                for (var i = 0; i < precision; i++)
                {
                    precisionStr.Append('0');
                }
                c.labelValue.Content = c.Value.ToString(precisionStr.ToString());
            }));

        private static readonly DependencyProperty AnimateProperty =
            DependencyProperty.Register("Animate", typeof(double), typeof(TitledRoundProcess), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as TitledRoundProcess;
                var value = (double)e.NewValue;
                c.arc.EndAngle = value / c.Limit * 360;
                var precisionStr = new StringBuilder("#0.");
                for (var i = 0; i < c.Precision; i++)
                {
                    precisionStr.Append('0');
                }
                c.labelValue.Content = value.ToString(precisionStr.ToString());
            }));

        public Theme.Colors ThemeColor
        {
            get { return (Theme.Colors)GetValue(ThemeColorProperty); }
            set { SetValue(ThemeColorProperty, value); }
        }

        public static readonly DependencyProperty ThemeColorProperty =
            DependencyProperty.Register("ThemeColor", typeof(Theme.Colors), typeof(TitledRoundProcess), new PropertyMetadata(Theme.Colors.Default));

    }
}
