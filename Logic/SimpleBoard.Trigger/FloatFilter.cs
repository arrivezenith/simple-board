﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace SimpleBoard.Trigger
{
    public class FloatFilter : Filter
    {
        public FilterValueCondition Condition { get; set; }

        public float StandardValue { get; set; }

        private float? previousValue;

        public override bool IsFitCondition(object propertyValue)
        {
            var value = (float)propertyValue;
            var expression = Condition switch
            {
                FilterValueCondition.Equal => value == StandardValue,
                FilterValueCondition.NotEqual => value != StandardValue,
                FilterValueCondition.Less => value - StandardValue < -1e-6,
                FilterValueCondition.Greater => value - StandardValue > 1e-6,
                FilterValueCondition.LessOrEqual => value <= StandardValue,
                FilterValueCondition.GreaterOrEqual => value >= StandardValue,
                FilterValueCondition.UpOver => previousValue < StandardValue && value > StandardValue,
                FilterValueCondition.DownOver => previousValue > StandardValue && value < StandardValue,
                _ => throw new NotImplementedException()
            };

            previousValue = value;
            return expression;
        }
    }
}
