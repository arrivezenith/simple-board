﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DSF.Net.Controllers.Ratio
{
    /// <summary>
    /// VertiRatio. 的交互逻辑
    /// </summary>
    public partial class VertiRatio : UserControl
    {
        public VertiRatio()
        {
            InitializeComponent();
            labelUp.MouseLeftButtonDown += Touch;
            labelDown.MouseLeftButtonDown += Touch;
        }

        public event EventHandler ValueChanged;

        private void Touch(object sender, MouseButtonEventArgs e)
        {
            Value = sender.Equals(labelUp) ? VertiRatioStatus.UpSelected : VertiRatioStatus.DownSelected;
            ValueChanged?.Invoke(this, null);
        }

        public string TextUp
        {
            get { return (string)GetValue(TextUpProperty); }
            set { SetValue(TextUpProperty, value); }
        }

        public string TextDown
        {
            get { return (string)GetValue(TextDownProperty); }
            set { SetValue(TextDownProperty, value); }
        }

        public VertiRatioStatus Value
        {
            get { return (VertiRatioStatus)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public static readonly DependencyProperty TextUpProperty =
            DependencyProperty.Register("TextUp", typeof(string), typeof(VertiRatio), new PropertyMetadata("FINISHED", (s, e) =>
            {
                var c = s as VertiRatio;
                c.labelUp.Content = e.NewValue;
            }));
        public static readonly DependencyProperty TextDownProperty =
            DependencyProperty.Register("TextDown", typeof(string), typeof(VertiRatio), new PropertyMetadata("UNFINISHED", (s, e) =>
            {
                var c = s as VertiRatio;
                c.labelDown.Content = e.NewValue;
            }));
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(VertiRatioStatus), typeof(VertiRatio), new PropertyMetadata(VertiRatioStatus.DownSelected, (s, e) =>
            {
                var c = s as VertiRatio;
                var value = (VertiRatioStatus)e.NewValue;
                var storyboard = value == VertiRatioStatus.UpSelected ? (c.Resources["SelectToUp"] as Storyboard) : (c.Resources["SelectToDown"] as Storyboard);
                storyboard.Begin();
            }));
    }
}
