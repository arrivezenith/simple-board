﻿using SimpleBoard.ControllerMeta;

namespace SimpleBoard.BasicControls
{
    [SimpleBoardController("基本文本框")]
    [HasConfigurableProperty(nameof(Content), PropertyType.String, "文本")]
    [HasConfigurableProperty(nameof(FontSize), PropertyType.Double, "字体大小")]
    public class Label : System.Windows.Controls.Label
    {
        public Label() : base()
        {
            Width = 200;
            Height = 100;
            Content = "基本文本框";
            VerticalContentAlignment = System.Windows.VerticalAlignment.Top;
            HorizontalContentAlignment = System.Windows.HorizontalAlignment.Left;
            Padding = new System.Windows.Thickness(0);
        }
    }
}
