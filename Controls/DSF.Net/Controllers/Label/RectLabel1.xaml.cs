﻿using DSF.Net.Controllers.Theme;
using SimpleBoard.ControllerMeta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DSF.Net.Controllers.Label
{
    /// <summary>
    /// RectLabel1.xaml 的交互逻辑
    /// </summary>
    [SimpleBoardController("DSF文本框-类型1")]
    [HasConfigurableProperty(nameof(Text), PropertyType.String, "文本")]
    public partial class RectLabel1 : UserControl,IUserTheme
    {
        public RectLabel1()
        {
            InitializeComponent();
            DataContext = this;
            this.Width = 105;
            this.Height = 27;
        }


        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(RectLabel1), new PropertyMetadata("Title", (s, e) =>
            {
                (s as RectLabel1).label.Content = e.NewValue;
            }));
        public Theme.Colors ThemeColor
        {
            get { return (Theme.Colors)GetValue(ThemeColorProperty); }
            set { SetValue(ThemeColorProperty, value); }
        }

        public static readonly DependencyProperty ThemeColorProperty =
            DependencyProperty.Register("ThemeColor", typeof(Theme.Colors), typeof(RectLabel1), new PropertyMetadata(Theme.Colors.Default));

    }
}
