﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBoard.ControllerMeta
{
    /// <summary>
    /// 属性信息
    /// </summary>
    public class PropertyInfo
    {
        public string Name { get; set; } = null!;
        public string Title { get; set; } = null!;
        public PropertyType Type { get; set; }
    }
}
