﻿using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SimpleBoard.Backend.Server.Entities.Users
{
    public class DeviceRestry
    {
        /// <summary>
        /// 设备种类编号
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        /// <summary>
        /// 设备种类名称
        /// </summary>
        public string Name { get; set; } = null!;

        /// <summary>
        /// 数据帧描述JSON
        /// </summary>
        public string DataframeDescription { get; set; } = null!;
    }
}
