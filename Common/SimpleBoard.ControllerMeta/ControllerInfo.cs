﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBoard.ControllerMeta
{
    /// <summary>
    /// 控件元信息
    /// </summary>
    public class ControllerInfo
    {
        public string Title { get; set; } = null!;
        public string AssemblyFullName { get; set; } = null!;
        public string TypeFullName { get; set; } = null!;
        public ICollection<PropertyInfo> Properties { get; set; } = null!;
    }
}
