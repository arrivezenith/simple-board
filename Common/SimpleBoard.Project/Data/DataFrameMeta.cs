﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace SimpleBoard.Project.Data
{
    public class DataFrameMeta
    {
        public IEnumerable<DataUnit> DataUnits { get; set; } = null!;

        public DataUnit this[int index]
        {
            get => DataUnits.Skip(index).First();
        }

        public static string Serialize(DataFrameMeta dataFrameMeta)
        {
            return JsonSerializer.Serialize(dataFrameMeta);
        }
    }
}
