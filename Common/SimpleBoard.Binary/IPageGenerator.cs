﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;

namespace SimpleBoard.Binary
{
    public interface IPageGenerator
    {
        Size GetWindowSize();
        void InitializeComponents(Panel root);
    }
}
