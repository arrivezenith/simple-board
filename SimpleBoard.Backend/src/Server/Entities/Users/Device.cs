﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SimpleBoard.Backend.Server.Entities.Users
{
    public class Device
    {
        /// <summary>
        /// 设备Id
        /// </summary>
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid DeviceId { get; set; }

        /// <summary>
        /// 设备名称
        /// </summary>
        public string Name { get; set; } = null!;

        /// <summary>
        /// 设备类型
        /// </summary>
        public DeviceRestry DeviceType { get; set; } = null!;

        /// <summary>
        /// 设备类型Id
        /// </summary>
        [ForeignKey(nameof(DeviceType))]
        public int DeviceTypeId { get; set; }
    }
}
