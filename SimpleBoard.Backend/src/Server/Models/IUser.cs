﻿using System;
using System.Collections.Generic;

namespace SimpleBoard.Backend.Server.Models
{
    public interface IUser
    {
        Guid UniqueUserId { get; }
        string UserType { get; }

        IDictionary<string, string> GetUserClaims();
    }
}
