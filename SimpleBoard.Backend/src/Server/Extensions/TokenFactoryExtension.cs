﻿using Microsoft.Extensions.DependencyInjection;
using SimpleBoard.Backend.Server.Configurations;
using SimpleBoard.Backend.Server.Entities;
using SimpleBoard.Backend.Server.Services.Auth;
using System;

namespace SimpleBoard.Backend.Server.Extensions
{
    public static class TokenFactoryExtension
    {
        public static IServiceCollection AddTokenFactory(this IServiceCollection services, Action<TokenFactoryConfiguration> options)
        {
            var config = new TokenFactoryConfiguration();
            options(config);
            if (config.Audience is null) throw new Exception(nameof(config.Audience));
            if (config.Issuer is null) throw new Exception(nameof(config.Issuer));
            if (config.SigningKey is null) throw new Exception(nameof(config.SigningKey));

            services.AddScoped<ITokenFactory, TokenFactory>(services =>
            {
                var dbContext = services.GetRequiredService<ApplicationDbContext>();
                return new TokenFactory(dbContext, config);
            });

            return services;
        }
    }
}
