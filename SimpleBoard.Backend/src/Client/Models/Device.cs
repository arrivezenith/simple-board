﻿namespace SimpleBoard.Backend.Client.Models
{
    public class Device
    {
        public string Name { get; set; }
        public string Id { get; set; }
        public string Type { get; set; }
    }
}
