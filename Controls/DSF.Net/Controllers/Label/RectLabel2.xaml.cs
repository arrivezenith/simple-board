﻿using DSF.Net.Controllers.Theme;
using SimpleBoard.ControllerMeta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DSF.Net.Controllers.Label
{
    /// <summary>
    /// RectLabel2.xaml 的交互逻辑
    /// </summary>
    [SimpleBoardController("DSF数值-类型2")]
    [HasConfigurableProperty(nameof(Text), PropertyType.String, "文本")]
    [HasConfigurableProperty(nameof(Value), PropertyType.Double, "数值")]
    [HasConfigurableProperty(nameof(Unit), PropertyType.String, "数值单位")]
    [HasConfigurableProperty(nameof(Precision), PropertyType.Int32, "保留小数")]
    public partial class RectLabel2 : UserControl,IUserTheme
    {
        public RectLabel2()
        {
            InitializeComponent();
            DataContext = this;
            this.Width = 130;
            this.Height = 73;
        }

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(RectLabel2), new PropertyMetadata("Data", (s, e) =>
            {
                (s as RectLabel2).typeLabel.Content = e.NewValue;
            }));


        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Value.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(double), typeof(RectLabel2), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as RectLabel2;
                var oldValue = (double)e.OldValue;
                var newValue = (double)e.NewValue;
                var animation = new DoubleAnimation
                {
                    From = oldValue,
                    To = newValue,
                    Duration = new Duration(new TimeSpan(0, 0, 0, 0, 500)),
                    EasingFunction = new QuadraticEase()
                };
                c.BeginAnimation(AnimateProperty, animation);
            }));

        public int Precision
        {
            get { return (int)GetValue(PrecisionProperty); }
            set { SetValue(PrecisionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Precision.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PrecisionProperty =
            DependencyProperty.Register("Precision", typeof(int), typeof(RectLabel2), new PropertyMetadata(2, (s, e) =>
            {
                var c = s as RectLabel2;
                var precisionStr = new StringBuilder("#0.");
                for (var i = 0; i < (int)e.NewValue; i++)
                {
                    precisionStr.Append('0');
                }
                c.valueLabel.Content = c.Value.ToString(precisionStr.ToString()) + c.Unit;
            }));


        public string Unit
        {
            get { return (string)GetValue(UnitProperty); }
            set { SetValue(UnitProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Unit.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty UnitProperty =
            DependencyProperty.Register("Unit", typeof(string), typeof(RectLabel2), new PropertyMetadata("A", (s, e) =>
            {
                var c = s as RectLabel2;
                var precisionStr = new StringBuilder("#0.");
                for (var i = 0; i < c.Precision; i++)
                {
                    precisionStr.Append('0');
                }
                c.valueLabel.Content = c.Value.ToString(precisionStr.ToString()) + e.NewValue;
            }));

        private static readonly DependencyProperty AnimateProperty =
            DependencyProperty.Register("Animate", typeof(double), typeof(RectLabel2), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as RectLabel2;
                var value = (double)e.NewValue;
                var precisionStr = new StringBuilder("#0.");
                for(var i =0;i<c.Precision;i++)
                {
                    precisionStr.Append('0');
                }
                c.valueLabel.Content = value.ToString(precisionStr.ToString()) + c.Unit;
            }));

        public Theme.Colors ThemeColor
        {
            get { return (Theme.Colors)GetValue(ThemeColorProperty); }
            set { SetValue(ThemeColorProperty, value); }
        }

        public static readonly DependencyProperty ThemeColorProperty =
            DependencyProperty.Register("ThemeColor", typeof(Theme.Colors), typeof(RectLabel2), new PropertyMetadata(Theme.Colors.Default));

    }
}
