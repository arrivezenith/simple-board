﻿using Microsoft.Toolkit.Mvvm.Messaging.Messages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SimpleBoard.Designer.Messages
{
    public record CanvasSizeUpdateRequest(int Width, int Height);
    public record CanvasBackgroundUpdateRequest(Brush Background);
    public class CanvasPropertyRequest : RequestMessage<CanvasProperty> { }
    public record ActiveElementChangedMessage(FrameworkElement? Element);
}
