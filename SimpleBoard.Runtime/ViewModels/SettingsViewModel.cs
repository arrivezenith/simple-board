﻿using SimpleBoard.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBoard.Runtime.ViewModels
{
    [InjectAs(InjectionType.Singleton)]
    public class SettingsViewModel
    {
        public SettingsViewModel()
        {
            SelectedProt = AvailablePorts.FirstOrDefault();
            SelectedDataParser = AvailableDataParsers.FirstOrDefault();
            ServerUrl = "https://localhost:5001";
        }

        /// <summary>
        /// 可用端口
        /// </summary>
        public IEnumerable<string> AvailablePorts { get; set; } = new List<string>()
        {
            "COM1", "COM2", "COM3"
        };

        public IEnumerable<string> AvailableDataParsers { get; set; } = new List<string>()
        {
            "逗号分隔文本解析器"
        };

        public string? SelectedProt { get; set; } 
        public string? SelectedDataParser { get; set; }

        public string? ServerUrl { get; set; }
        public string? Uid { get; set; }
        /// <summary>
        /// 波特率
        /// </summary>
        public int BoundRate { get; set; } = 115200;
    }
}
