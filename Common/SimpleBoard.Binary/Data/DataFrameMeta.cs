﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace SimpleBoard.Binary.Data
{
    public class DataFrameMeta
    {
        public IEnumerable<DataUnit> DataUnits { get; set; } = null!;

        public DataUnit this[int index]
        {
            get => DataUnits.Skip(index).First();
        }

        public static DataFrameMeta Deserialize(string json)
        {
            return JsonSerializer.Deserialize<DataFrameMeta>(json) ?? throw new InvalidCastException();
        }
    }
}
