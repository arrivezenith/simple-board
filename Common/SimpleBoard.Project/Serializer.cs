﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;

namespace SimpleBoard.Project
{
    public class Serializer
    {
        public static string Serialize(ProjectInfo project) => JsonSerializer.Serialize<ProjectInfo>(project, new JsonSerializerOptions
        {
            NumberHandling = System.Text.Json.Serialization.JsonNumberHandling.WriteAsString
        });

        public static ProjectInfo Deserialize(string json) => JsonSerializer.Deserialize<ProjectInfo>(json, new JsonSerializerOptions
        {
            NumberHandling = System.Text.Json.Serialization.JsonNumberHandling.AllowReadingFromString
        })!;
    }
}
