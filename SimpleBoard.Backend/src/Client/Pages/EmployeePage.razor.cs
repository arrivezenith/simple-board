﻿using Microsoft.AspNetCore.Components;
using SimpleBoard.Backend.Client.Models;
using SimpleBoard.Backend.Client.Services;
using System;
using System.Linq;
using System.Threading.Tasks;

namespace SimpleBoard.Backend.Pages
{
    public partial class EmployeePage
    {
        private Employee[] _data = Array.Empty<Employee>();

        private void ShowModal()
        {
        }

        [Inject] protected IEmployeeService EmployeeService { get; set; }

        protected override async Task OnInitializedAsync()
        {
            await base.OnInitializedAsync();
            var apiResult = await EmployeeService.GetEmployeeAsync();
            _data = apiResult.Data;
        }
    }
}
