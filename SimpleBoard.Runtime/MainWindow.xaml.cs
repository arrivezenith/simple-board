﻿using Microsoft.Toolkit.Mvvm.Messaging;
using SimpleBoard.Binary;
using SimpleBoard.DependencyInjection;
using SimpleBoard.Runtime.Messages;
using SimpleBoard.Runtime.Pages;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SimpleBoard.Runtime
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    [InjectAs(InjectionType.Singleton)]
    public partial class MainWindow : Window, IRecipient<NavigationRequestMessage>
    {
        public MainWindow(MainPage mainPage, SettingsPage settingsPage)
        {
            InitializeComponent();

            WeakReferenceMessenger.Default.Register<NavigationRequestMessage>(this);

            // 初始化组件
            App.AutoWired<IPageGenerator>().InitializeComponents(mainPage.AppRoot);

            // 设置布局
            foreach (FrameworkElement element in mainPage.AppRoot.Children)
            {
                element.HorizontalAlignment = HorizontalAlignment.Left;
                element.VerticalAlignment = VerticalAlignment.Top;
            }

            this.frame.Navigate(settingsPage);
        }

        ~MainWindow()
        {
            WeakReferenceMessenger.Default.UnregisterAll(this);
        }

        public void Receive(NavigationRequestMessage message)
        {
            this.frame.Navigate(message.page);
        }

        private void Window_Closed(object sender, EventArgs e)
        {
            Environment.Exit(Environment.ExitCode);
        }
    }
}
