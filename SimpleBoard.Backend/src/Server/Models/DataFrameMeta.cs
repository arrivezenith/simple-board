﻿using System.Collections.Generic;
using System.Linq;

namespace SimpleBoard.Backend.Server.Models
{
    public class DataFrameMeta
    {
        public IEnumerable<DataUnit> DataUnits { get; set; } = null!;

        public DataUnit this[int index]
        {
            get => DataUnits.Skip(index).First();
        }
    }

    public enum DataUnitType
    {
        Int32,
        Float32,
        String,
        Bool
    }

    public class DataUnit
    {
        public string Name { get; set; } = null!;
        public DataUnitType Type { get; set; }
    }
}
