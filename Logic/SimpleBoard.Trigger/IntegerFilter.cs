﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBoard.Trigger
{
    public class IntegerFilter : Filter
    {   
        public FilterValueCondition Condition { get; set; }

        public int StandardValue { get; set; }

        private int? previousValue;

        public override bool IsFitCondition(object propertyValue)
        {
            var value = (int)propertyValue;
            var expression = Condition switch
            {
                FilterValueCondition.Equal => value == StandardValue,
                FilterValueCondition.NotEqual => value != StandardValue,
                FilterValueCondition.Less => value < StandardValue,
                FilterValueCondition.Greater => value > StandardValue,
                FilterValueCondition.LessOrEqual => value <= StandardValue,
                FilterValueCondition.GreaterOrEqual => value >= StandardValue,
                FilterValueCondition.UpOver => previousValue < StandardValue && value > StandardValue,
                FilterValueCondition.DownOver => previousValue > StandardValue && value < StandardValue,
                _ => throw new NotImplementedException()
            };

            previousValue = value;
            return expression;
        }
    }
}
