using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using SimpleBoard.Backend.Server.Entities;
using SimpleBoard.Backend.Server.Extensions;
using SimpleBoard.Backend.Server.Services;
using SimpleBoard.Backend.Server.Services.Host;
using System;
using System.IO;
using System.Reflection;
using System.Text;

namespace SimpleBoard.Backend.Server
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddControllersWithViews();
            services.AddRazorPages();
            // 数据库
            services.AddDbContext<ApplicationDbContext>(opt =>
            {
                opt.UseMySql(Configuration.GetConnectionString("DbContext"),
                     ServerVersion.AutoDetect(Configuration.GetConnectionString("DbContext")));
            });
            // 缓存
            services.AddMemoryCache();
            // JWT签发
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(options =>
                    {
                        var jwtConfig = Configuration.GetSection("Jwt");
                        options.TokenValidationParameters = new()
                        {
                            ValidateIssuer = true,
                            ValidateAudience = true,
                            ValidateLifetime = true,
                            ClockSkew = TimeSpan.FromSeconds(30),
                            ValidateIssuerSigningKey = true,
                            ValidAudience = jwtConfig.GetValue<string>("Audience"),
                            ValidIssuer = jwtConfig.GetValue<string>("Issuer"),
                            IssuerSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(jwtConfig.GetValue<string>("SecurityKey")))
                        };
                    });
            // Token签发服务
            services.AddTokenFactory(options =>
            {
                var config = Configuration.GetSection("Jwt");
                options.Issuer = config.GetValue<string>("Issuer");
                options.Audience = config.GetValue<string>("Audience");
                options.SigningKey = config.GetValue<string>("SecurityKey");
                options.AccessTokenExpire = config.GetValue<int>("AccessTokenExpire");
                options.RefreshTokenExpire = config.GetValue<int>("RefreshTokenExpire");
                options.RefreshTokenBefore = config.GetValue<int>("RefreshTokenBefore");
            });
            // 用户密钥管理服务
            services.AddUserSecretManager(options =>
            {
                var config = Configuration.GetSection("UserSecret");
                options.SymmetricKey = config.GetValue<string>("SymmetricKey");
                options.SymmetricSalt = config.GetValue<string>("SymmetricSalt");
                options.HashSalt = config.GetValue<string>("HashSalt");
            });

            services.AddTransient<IDeviceService, DeviceService>();
            services.AddSingleton<TriggerService>();

            // Swagger
            services.AddEndpointsApiExplorer();
            services.AddSwaggerGen(c =>
            {
                var xmlFile = $"{Assembly.GetExecutingAssembly().GetName().Name}.xml";
                var xmlPath = Path.Combine(AppContext.BaseDirectory, xmlFile);
                c.IncludeXmlComments(xmlPath);
            });

            services.AddHostedService<TriggerExecuteService>();
            services.AddHostedService<TriggerMonitorService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseSwagger();
                app.UseSwaggerUI();
            }
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
                app.UseWebAssemblyDebugging();
            }
            else
            {
                app.UseExceptionHandler("/Error");
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            app.UseHttpsRedirection();

            app.UseBlazorFrameworkFiles();
            app.UseStaticFiles();

            app.UseRouting();

            app.UseAuthentication();
            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapRazorPages();
                endpoints.MapControllers();
                endpoints.MapFallbackToFile("index.html");
            });
        }
    }
}
