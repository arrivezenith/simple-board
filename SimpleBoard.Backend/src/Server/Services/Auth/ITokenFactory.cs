﻿using SimpleBoard.Backend.Server.Models;

namespace SimpleBoard.Backend.Server.Services.Auth
{
    public interface ITokenFactory
    {
        int RefreshTokenExpireBefore { get; }
        string CreateAccessToken(IUser user);
        string CreateRefreshToken(IUser user);
    }
}
