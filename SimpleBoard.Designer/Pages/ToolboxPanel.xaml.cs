﻿using SimpleBoard.DependencyInjection;
using SimpleBoard.Designer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SimpleBoard.Designer.Pages
{
    /// <summary>
    /// ToolboxPanel.xaml 的交互逻辑
    /// </summary>
    [InjectAs(InjectionType.Singleton)]
    public partial class ToolboxPanel : Page
    {
        public ToolboxPanel(ControllerService controllerService)
        {
            InitializeComponent();
            this.controllerService = controllerService;
            this.ControllerList.ItemsSource = controllerService.Controllers;
        }

        private readonly ControllerService controllerService;

        private void ControllerList_MouseMove(object sender, MouseEventArgs e)
        {
            if (e.LeftButton == MouseButtonState.Pressed && ControllerList.SelectedItems.Count > 0)
            {
                DragDrop.DoDragDrop(this, ControllerList.SelectedItem, DragDropEffects.Move);
            }
        }

    }
}
