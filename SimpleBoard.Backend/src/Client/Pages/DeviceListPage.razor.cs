﻿using Microsoft.AspNetCore.Components;
using SimpleBoard.Backend.Client.Models;
using SimpleBoard.Backend.Client.Services;
using System;
using System.Threading.Tasks;

namespace SimpleBoard.Backend.Pages
{
    public partial class DeviceListPage
    {
        private Device[] _data = Array.Empty<Device>();

        private void ShowModal()
        {
            NavigationManager.NavigateTo("/device/list/add"); 
        }

        [Inject] protected IDeviceService DeviceService { get; set; }
        [Inject] protected NavigationManager NavigationManager { get; set; }

        protected override async Task OnInitializedAsync()
        {
            await base.OnInitializedAsync();
            var apiResult = await DeviceService.GetDeviceAsync();
            _data = apiResult.Data;
        }
    }
}
