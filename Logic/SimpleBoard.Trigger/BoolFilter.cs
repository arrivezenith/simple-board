﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBoard.Trigger
{
    public class BoolFilter : Filter
    {
        public bool StandardValue { get; set; }

        public override bool IsFitCondition(object propertyValue)
        {
            var value = (bool)propertyValue;
            return value == StandardValue;
        }
    }
}
