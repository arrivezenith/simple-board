﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace DSF.Net.Controllers.Theme
{
    public interface IUserTheme
    {
        Colors ThemeColor { get; set; }
    }
}
