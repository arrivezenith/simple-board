﻿using SimpleBoard.Backend.Client.Models;
using System.Net.Http;
using System.Net.Http.Json;
using System.Threading.Tasks;

namespace SimpleBoard.Backend.Client.Services
{
    public interface IEmployeeService
    {
        Task<ApiResult<Employee[]>> GetEmployeeAsync();
    }

    public class EmployeeService : IEmployeeService
    {
        private readonly HttpClient _httpClient;

        public EmployeeService(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        public async Task<ApiResult<Employee[]>> GetEmployeeAsync()
        {
            return await _httpClient.GetFromJsonAsync<ApiResult<Employee[]>>("api/employee/getEmployeeList");
        }
    }
}
