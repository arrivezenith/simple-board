﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBoard.ControllerMeta
{
    [AttributeUsage(AttributeTargets.Class)]
    public class SimpleBoardControllerAttribute : Attribute
    {
        public SimpleBoardControllerAttribute(string title)
        {
            Title = title;
        }

        public string Title { get; set; } = null!;
    }
}
