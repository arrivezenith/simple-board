﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SimpleBoard.Backend.Server.Entities.Users
{
    public class UserClaim
    {
        [Key]
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public int Id { get; set; }

        [Required]
        public string ClaimType { get; set; } = null!;

        [Required]
        public string ClaimValue { get; set; } = null!;

        public Guid UserId { get; set; }
    }
}
