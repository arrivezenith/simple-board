﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBoard.Compiler.SourceCodeGenerator
{
    internal interface ISourceCodeGenerator
    {
        public string GenerateSourceCode();
        public IEnumerable<string> GetAssemblies();
    }
}
