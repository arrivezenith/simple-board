﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;

namespace SimpleBoard.Binary
{
    public static class BindableProperty
    {
        public static readonly Dictionary<string, List<BindingContext>> registerdBindings = new();

        public static void Register(string datasourceName, FrameworkElement element, string propertyName)
        {
            if (!registerdBindings.ContainsKey(datasourceName))
            {
                registerdBindings.Add(datasourceName, new List<BindingContext>());
            }
            registerdBindings[datasourceName].Add(new BindingContext(element, propertyName));
        }

        public static IEnumerable<BindingContext> GetBindings(string dataUnitName)
        {
            if (registerdBindings.ContainsKey(dataUnitName))
                return registerdBindings[dataUnitName];
            else
                return Enumerable.Empty<BindingContext>();
        }
    }

    public record BindingContext(FrameworkElement Element, string PropertyName);
}
