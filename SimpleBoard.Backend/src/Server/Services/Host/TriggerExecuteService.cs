﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System.Threading;
using System.Threading.Tasks;

namespace SimpleBoard.Backend.Server.Services.Host
{
    public class TriggerExecuteService : BackgroundService
    {
        public TriggerExecuteService(TriggerService triggerService, ILogger<TriggerExecuteService> logger)
        {
            this.triggerService = triggerService;
            this.logger = logger;
        }

        private readonly TriggerService triggerService;
        private readonly ILogger logger;

        protected override Task ExecuteAsync(CancellationToken stoppingToken)
        {
            return Task.Run(() =>
            {
                foreach (var trigger in triggerService.ExecutingTriggers.GetConsumingEnumerable(stoppingToken))
                {
                    // TODO 执行trigger
                    logger.LogInformation("触发器 {name} 被触发.", trigger.Name);
                }
            }, stoppingToken);
        }
    }
}
