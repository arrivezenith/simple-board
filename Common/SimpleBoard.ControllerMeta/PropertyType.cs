﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBoard.ControllerMeta
{
    /// <summary>
    /// 可配置的属性类型
    /// </summary>
    public enum PropertyType
    {
        String,
        Int32,
        Double,
        Bool,
    }
}
