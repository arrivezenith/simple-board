﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBoard.Project.UI
{
    public class ControlInfo
    {
        public ControlInfo()
        {
            this.Properties = new();
        }
        public string AssemblyFullName { get; set; } = null!;
        public string TypeFullName { get; set; } = null!;

        public string Name { get; set; } = null!;
        public int Width { get; set; }
        public int Height { get; set; }
        public int Top { get; set; }
        public int Left { get; set; }
        public Dictionary<string, object> Properties { get; set; }

    }
}
