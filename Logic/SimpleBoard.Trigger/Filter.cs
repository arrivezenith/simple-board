﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBoard.Trigger
{
    public abstract class Filter
    {
        /// <summary>
        /// 被过滤的属性名称
        /// </summary>
        public string TargetPropertyName { get; set; } = null!;

        /// <summary>
        /// 判断属性值是否满足条件
        /// </summary>
        /// <returns></returns>
        public abstract bool IsFitCondition(object propertyValue);
    }
}
