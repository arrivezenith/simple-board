﻿// See https://aka.ms/new-console-template for more information
//using SimpleBoard.Trigger;
//using System.Xml.Serialization;

//var trigger = new Trigger
//{
//    Action = "test",
//    ActionParameters = new[] { "ttt", "eee" },
//    Interval = 15,
//    IsActivated = false,
//    IsEnabled = false,
//    TargetId = Guid.NewGuid(),
//    TargetType = "12341351",
//    Filters = new Filter[]
//    {
//        new FloatFilter { Condition= FilterValueCondition.NotEqual, StandardValue = 12, TargetPropertyName = "test" },
//        new IntegerFilter { Condition= FilterValueCondition.NotEqual, StandardValue = 12, TargetPropertyName = "test" },
//    }
//};

//var xml = SerializeXml(trigger);

//var trigger2 = DeserializeXml(xml);

//Console.WriteLine(xml);

//string SerializeXml(object data)
//{
//    using (StringWriter sw = new StringWriter())
//    {
//        XmlSerializer xz = new XmlSerializer(data.GetType());
//        xz.Serialize(sw, data);
//        return sw.ToString();
//    }
//}

//Trigger DeserializeXml(string xml)
//{
//    using (StringReader sr = new StringReader(xml))
//    {
//        XmlSerializer xz = new XmlSerializer(typeof(Trigger));
//        return xz.Deserialize(sr) as Trigger ?? throw new InvalidCastException();
//    }
//}

using Microsoft.Extensions.Caching.Memory;

IMemoryCache cache = new Microsoft.Extensions.Caching.Memory.MemoryCache(new MemoryCacheOptions());

var hit = cache.TryGetValue("test", out var value);
value = new object();
hit = cache.TryGetValue("test", out var value2);
Console.WriteLine(hit);
Console.WriteLine(value2 is null);