﻿using DSF.Net.Controllers.Theme;
using SimpleBoard.ControllerMeta;
using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
namespace DSF.Net.Controllers.Process
{
    [SimpleBoardController("DSF仪表-圆形指针")]
    [HasConfigurableProperty(nameof(Value), PropertyType.Double, "数值")]
    [HasConfigurableProperty(nameof(Interval), PropertyType.Int32, "刻度间隔")]
    [HasConfigurableProperty(nameof(Text), PropertyType.String, "文本")]
    [HasConfigurableProperty(nameof(Unit), PropertyType.String, "单位")]
    public partial class RotateProcessHalf : UserControl,IUserTheme
    {
        public RotateProcessHalf()
        {
            InitializeComponent();
            DataContext = this;
            Width = 140;
            Height = 140;
        }
        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        private static readonly DependencyProperty ArcAnimationProperty =
            DependencyProperty.Register("ArcAnimation", typeof(double), typeof(RotateProcessHalf), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as RotateProcessHalf;
                var value = (double)e.NewValue;
                var full = c.Interval * 7;
                c.Arc.EndAngle = -120.5 + 240 * value / full;
            }));

        private static readonly DependencyProperty PointerAnimationProperty =
            DependencyProperty.Register("PointerAnimation", typeof(double), typeof(RotateProcessHalf), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as RotateProcessHalf;
                var value = (double)e.NewValue;
                var full = c.Interval * 7;
                c.Pointer.RenderTransform = new RotateTransform(-120 + 240 * value / full);
            }));
        private static readonly DependencyProperty ValueAnimationProperty =
            DependencyProperty.Register("ValueAnimation", typeof(double), typeof(RotateProcessHalf), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as RotateProcessHalf;
                var value = (double)e.NewValue;
                c.Speed.Content = $"{(int)value}{c.Unit}";
            }));
        // Using a DependencyProperty as the backing store for Value.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(double), typeof(RotateProcessHalf), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as RotateProcessHalf;
                var animate = new DoubleAnimation
                {
                    From = (double)e.OldValue,
                    To = (double)e.NewValue,
                    Duration = new Duration(new TimeSpan(0, 0, 0, 0, 800)),
                    EasingFunction = new QuarticEase()
                };
                c.BeginAnimation(ArcAnimationProperty, animate);
                c.BeginAnimation(PointerAnimationProperty, animate);
                c.BeginAnimation(ValueAnimationProperty, animate);
            }));
        public int Interval
        {
            get { return (int)GetValue(IntervalProperty); }
            set { SetValue(IntervalProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Interval.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty IntervalProperty =
            DependencyProperty.Register("Interval", typeof(int), typeof(RotateProcessHalf), new PropertyMetadata(100, (s, e) =>
            {
                var c = s as RotateProcessHalf;
                var value = (int)e.NewValue;
                c.scale1.Content = value;
                c.scale2.Content = value * 2;
                c.scale3.Content = value * 3;
                c.scale4.Content = value * 4;
                c.scale5.Content = value * 5;
                c.scale6.Content = value * 6;
                c.scale7.Content = value * 7;
                var full = value * 7;
                c.Arc.EndAngle = -120.5 + 240 * c.Value / full;
                c.Pointer.RenderTransform = new RotateTransform(-120 + 240 * c.Value / full);
            }));

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(RotateProcessHalf), new PropertyMetadata("Speed", (s, e) =>
            {
                var c = s as RotateProcessHalf;
                c.Title.Content = e.NewValue;
            }));

        public string Unit
        {
            get { return (string)GetValue(UnitProperty); }
            set { SetValue(UnitProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Unit.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty UnitProperty =
            DependencyProperty.Register("Unit", typeof(string), typeof(RotateProcessHalf), new PropertyMetadata("kmH", (s, e) =>
            {
                var c = s as RotateProcessHalf;
                c.Speed.Content = $"{c.Value}{e.NewValue}";
            }));

        public Theme.Colors ThemeColor
        {
            get { return (Theme.Colors)GetValue(ThemeColorProperty); }
            set { SetValue(ThemeColorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ThemeColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ThemeColorProperty =
            DependencyProperty.Register("ThemeColor", typeof(Theme.Colors), typeof(RotateProcessHalf), new PropertyMetadata(Theme.Colors.Default));

    }
}
