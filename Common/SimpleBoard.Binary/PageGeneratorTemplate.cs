﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;

namespace SimpleBoard.Binary
{
    internal class PageGeneratorTemplate : IPageGenerator
    {
        public Size GetWindowSize() => new (800, 450);

        public void InitializeComponents(Panel root)
        {
            // 设置背景颜色
            root.Background = new SolidColorBrush((Color)ColorConverter.ConvertFromString("#00002B"));
            // 依次添加控件并设置大小、位置、属性
            var control1 = new Label()
            {
                Width = 200,
                Height = 100,
                Content = "测试测试测试",
                FontSize = 18,
                Margin = new Thickness(20, 30, 0, 0),
            };
            BindableProperty.Register("温度", control1, nameof(control1.Content));
            root.Children.Add(control1);
        }
    }
}
