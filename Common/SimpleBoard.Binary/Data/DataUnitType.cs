﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBoard.Binary.Data
{
    public enum DataUnitType
    {
        Int32,
        Float32,
        String,
        Bool
    }
}
