﻿using DSF.Net.Controllers.Theme;
using SimpleBoard.ControllerMeta;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DSF.Net.Controllers.Process
{
    /// <summary>
    /// HoriProcessHalf.xaml 的交互逻辑
    /// </summary>
    [SimpleBoardController("DSF指示器-水平单向文本")]
    [HasConfigurableProperty(nameof(Value), PropertyType.Double, "数值")]
    [HasConfigurableProperty(nameof(Limit), PropertyType.Double, "区间长度")]
    [HasConfigurableProperty(nameof(Text), PropertyType.String, "文本")]
    [HasConfigurableProperty(nameof(Unit), PropertyType.String, "数值单位")]
    [HasConfigurableProperty(nameof(Precision), PropertyType.Int32, "保留小数")]
    public partial class HoriProcessHalf : UserControl, IUserTheme
    {
        public HoriProcessHalf()
        {
            InitializeComponent();
            Width = 300;
            Height = 30;
        }

        readonly int barLenth = 300;

        public string Text
        {
            get { return (string)GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(HoriProcessHalf), new PropertyMetadata("Data", (s, e) =>
            {
                var c = s as HoriProcessHalf;
                c.PointNameLabel.Content = e.NewValue;
            }));



        public string Unit
        {
            get { return (string)GetValue(UnitProperty); }
            set { SetValue(UnitProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Unit.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty UnitProperty =
            DependencyProperty.Register("Unit", typeof(string), typeof(HoriProcessHalf), new PropertyMetadata("m", (s, e) =>
            {
                var c = s as HoriProcessHalf;
                c.ValueLabel.Content = $"{((string)c.ValueLabel.Content).Replace((string)e.OldValue, "")}{e.NewValue}";
            }));

        public double Limit
        {
            get { return (double)GetValue(LimitProperty); }
            set { SetValue(LimitProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Limit.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LimitProperty =
            DependencyProperty.Register("Limit", typeof(double), typeof(HoriProcessHalf), new PropertyMetadata(100d, (s, e) => {
                var c = s as HoriProcessHalf;
                var value = (double)e.NewValue;
                c.Rec.Width = c.Value / value * c.barLenth;
            }));

        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        public int Precision
        {
            get { return (int)GetValue(PrecisionProperty); }
            set { SetValue(PrecisionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Precision.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PrecisionProperty =
            DependencyProperty.Register("Precision", typeof(int), typeof(HoriProcessHalf), new PropertyMetadata(2, (s, e) =>
            {
                var c = s as HoriProcessHalf;
                var value = (int)e.NewValue;
                var priciseStr = new StringBuilder("#0.");
                for (var i = 0; i < value; i++)
                {
                    priciseStr.Append('0');
                }
                c.ValueLabel.Content = c.Value.ToString(priciseStr.ToString()) + c.Unit;
            }));

        public Theme.Colors ThemeColor { get; set; } = Theme.Colors.Default;

        // Using a DependencyProperty as the backing store for Value.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(double), typeof(HoriProcessHalf), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as HoriProcessHalf;
                var oldValue = (double)e.OldValue;
                var newValue = (double)e.NewValue;
                var animation = new DoubleAnimation
                {
                    From = oldValue,
                    To = newValue,
                    Duration = new Duration(new TimeSpan(0, 0, 0, 0, 500)),
                    EasingFunction = new QuadraticEase()
                };
                c.BeginAnimation(BarAnimateProperty, animation);
                c.BeginAnimation(TextAnimateProperty, animation);
            }));

        private static readonly DependencyProperty BarAnimateProperty =
            DependencyProperty.Register("BarAnimate", typeof(double), typeof(HoriProcessHalf), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as HoriProcessHalf;
                var value = (double)e.NewValue;
                c.Rec.Fill = value > 0 ? c.ThemeColor.Light : c.ThemeColor.TextHighLight;
                c.Rec.Width = Math.Abs(value) / c.Limit * c.barLenth;
            }));
        private static readonly DependencyProperty TextAnimateProperty =
            DependencyProperty.Register("TextAnimate", typeof(double), typeof(HoriProcessHalf), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as HoriProcessHalf;
                var value = (double)e.NewValue;
                var priciseStr = new StringBuilder("#0.");
                for (var i = 0; i < c.Precision; i++)
                {
                    priciseStr.Append('0');
                }
                c.ValueLabel.Content = value.ToString(priciseStr.ToString()) + c.Unit;
            }));
    }
}
