﻿using Microsoft.Toolkit.Mvvm.Messaging;
using SimpleBoard.DependencyInjection;
using SimpleBoard.Runtime.Messages;
using SimpleBoard.Runtime.Services;
using SimpleBoard.Runtime.ViewModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SimpleBoard.Runtime.Pages
{
    /// <summary>
    /// SettingsPage.xaml 的交互逻辑
    /// </summary>
    [InjectAs(InjectionType.Singleton)]
    public partial class SettingsPage : Page
    {
        public SettingsPage(SettingsViewModel viewModel)
        {
            InitializeComponent();
            this.DataContext = viewModel;
            this.viewModel = viewModel;
        }

        private readonly SettingsViewModel viewModel;

        private void Ok_Click(object sender, RoutedEventArgs e)
        {
            var dataSource = new SerialPortDataSource(viewModel.SelectedProt!, viewModel.BoundRate)
            {
                ServerUrl = viewModel.ServerUrl,
                Uid = viewModel.Uid,
            };
            dataSource.StartReadData();
            WeakReferenceMessenger.Default.Send(new NavigationRequestMessage(App.AutoWired<MainPage>()));
        }
    }
}
