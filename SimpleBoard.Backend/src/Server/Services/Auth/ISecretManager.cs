﻿using System;

namespace SimpleBoard.Backend.Server.Services.Auth
{
    public interface ISecretManager
    {
        /// <summary>
        /// 绑定
        /// </summary>
        /// <param name="userId">要绑定的对象</param>
        void Bind(Guid userId);

        /// <summary>
        /// 获取指定名称的密钥
        /// </summary>
        /// <param name="secretName">密钥名称</param>
        /// <returns>密钥内容</returns>
        string GetSymmetricSecret(string secretName);

        /// <summary>
        /// 存储哈希密钥
        /// </summary>
        /// <param name="secretKey"></param>
        /// <param name="secret"></param>
        void StorageHashSecret(string secretKey, string secret);

        /// <summary>
        /// 哈希对比
        /// </summary>
        /// <param name="rawSecret">明文密码</param>
        /// <param name="hash">哈希值</param>
        /// <returns>是否匹配</returns>
        bool HashCompare(string rawSecret, string hash);

    }
}
