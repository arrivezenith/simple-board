﻿using Microsoft.AspNetCore.Mvc;
using System.Threading.Tasks;

namespace SimpleBoard.Backend.Server.Models
{
    public class ApiResult<T>
    {
        public int Code { get; set; }
        public string Message { get; set; } = null!;
        public T? Data { get; set; }
    }
}
