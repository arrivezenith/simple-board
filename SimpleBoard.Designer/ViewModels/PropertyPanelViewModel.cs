﻿using Microsoft.Toolkit.Mvvm.ComponentModel;
using Microsoft.Toolkit.Mvvm.Messaging;
using SimpleBoard.DependencyInjection;
using SimpleBoard.Designer.Messages;
using SimpleBoard.Designer.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Media;

namespace SimpleBoard.Designer.ViewModels
{
    [InjectAs(InjectionType.Transient)]
    public class PropertyPanelViewModel : ObservableRecipient
    {
        private int pageWidth;
        private int pageHeight;
        private Brush pageBackground = Brushes.White;

        private int elementWidth;
        private int elementHeight;
        private int elementLeft;
        private int elementTop;
        private string elementType = "";

        private readonly ControllerService controllerService;

        public PropertyPanelViewModel(ControllerService controllerService)
        {
            IsActive = true;
            var request = Messenger.Send(new CanvasPropertyRequest());
            var pageProps = request.HasReceivedResponse ? request.Response : null;
            this.pageWidth = pageProps?.CanvasWidth ?? 0;
            this.pageHeight = pageProps?.CanvasHeight ?? 0;
            this.pageBackground = pageProps?.Background ?? Brushes.White;
            this.controllerService = controllerService;
        }

        ~PropertyPanelViewModel()
        {
            IsActive = false;
        }

        public int PageWidth
        {
            get => pageWidth;
            set { SetProperty(ref pageWidth, value); 
                Messenger.Send(new CanvasSizeUpdateRequest(value, 0)); }
        }

        public int PageHeight
        {
            get => pageHeight;
            set { SetProperty(ref pageHeight, value); Messenger.Send(new CanvasSizeUpdateRequest(0, value)); }
        }

        public int ElementWidth
        {
            get => elementWidth;
            set { SetProperty(ref elementWidth, value); controllerService.SetWidth(TargetElement, value); }
        }

        public int ElementHeight
        {
            get => elementHeight;
            set { SetProperty(ref elementHeight, value); controllerService.SetHeight(TargetElement, value); }
        }

        public int ElementLeft
        {
            get => elementLeft;
            set { SetProperty(ref elementLeft, value); controllerService.SetLeft(TargetElement, value); }
        }

        public int ElementTop
        {
            get => elementTop;
            set { SetProperty(ref elementTop, value); controllerService.SetTop(TargetElement, value); }
        }

        public Brush PageBackground
        {
            get => pageBackground;
            set { SetProperty(ref pageBackground, value); Messenger.Send(new CanvasBackgroundUpdateRequest(PageBackground)); }
        }

        public string ElementType
        {
            get => elementType;
            set => SetProperty(ref elementType, value);
        }

        public FrameworkElement TargetElement { get; set; } = null!;
    }
}
