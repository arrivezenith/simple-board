﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace SimpleBoard.Designer.Controls
{
    /// <summary>
    /// SideTabButton.xaml 的交互逻辑
    /// </summary>
    public partial class SideTabButton : UserControl
    {
        public SideTabButton()
        {
            InitializeComponent();
            DataContext = this;
        }

        public string Title
        {
            get { return (string)GetValue(TitleProperty); }
            set { SetValue(TitleProperty, value); }
        }

        private static readonly Brush activeBackground = new SolidColorBrush(Color.FromRgb(0, 108, 190));
        private static readonly Brush inActiveBackground = new SolidColorBrush(Color.FromRgb(204, 206, 219));

        // Using a DependencyProperty as the backing store for Title.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TitleProperty =
            DependencyProperty.Register("Title", typeof(string), typeof(SideTabButton), new PropertyMetadata("新标签"));



        public double Angle
        {
            get { return (double)GetValue(AngleProperty); }
            set { SetValue(AngleProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Angle.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty AngleProperty =
            DependencyProperty.Register("Angle", typeof(double), typeof(SideTabButton), new PropertyMetadata(90d));


        private void Button_MouseEnter(object sender, MouseEventArgs e)
        {
            TitleLabel.Foreground = activeBackground;
            Bar.Fill = activeBackground;
        }

        private void Button_MouseLeave(object sender, MouseEventArgs e)
        {
            TitleLabel.Foreground = Brushes.Black;
            Bar.Fill = inActiveBackground;
        }
    }
}
