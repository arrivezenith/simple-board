﻿using SimpleBoard.Project.Binding;
using SimpleBoard.Project.Data;
using SimpleBoard.Project.UI;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBoard.Project
{
    public class ProjectInfo
    {
        public PageInfo Page { get; set; } = null!;
        public DataFrameMeta DataFrameMeta { get; set; } = null!;
        public IEnumerable<DataBinding> DataBindings { get; set; } = null!;
    }
}
