﻿using System.Windows.Media;

namespace DSF.Net.Controllers.Theme
{
    public class Colors
    {
        public Brush TextEnabled { get; set; }
        public Brush TextDisabled { get; set; }
        public Brush TextHighLight { get; set; }
        public Brush Light { get; set; }
        public Brush Dark { get; set; }
        public Brush Stroke { get; set; }
        public Brush Alert { get; set; }
        /// <summary>
        /// 默认主题
        /// </summary>
        public static Colors Default 
        {
            get => new Colors
            {
                //灰色，Dark，禁用文字
                TextDisabled = new SolidColorBrush(Color.FromRgb(129, 129, 129)),
                //白色，Light， 常规文字
                TextEnabled = new SolidColorBrush(Color.FromRgb(255, 255, 255)),
                //橙色，Ligtht，图形填充
                TextHighLight = new SolidColorBrush(Color.FromRgb(247, 181, 44)),
                
                //蓝绿色，Light，描边、填充
                Light = new SolidColorBrush(Color.FromRgb(59, 185, 187)),
                //紫色，Light，填充
                Dark = new SolidColorBrush(Color.FromRgb(47, 32, 50)),

                //蓝色，Light，描边、范围
                Stroke = new SolidColorBrush(Color.FromRgb(105, 196, 214)),

                //红色，警告、填充
                Alert =new SolidColorBrush(Color.FromRgb(231,54,13)),
            };
        }
    }
}