﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using SimpleBoard.Backend.Server.Entities;
using SimpleBoard.Backend.Server.Extensions;
using SimpleBoard.Backend.Server.Models;
using SimpleBoard.Backend.Server.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text.Json;
using System.Threading.Tasks;

namespace SimpleBoard.Backend.Server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class InteractiveController : ControllerBase
    {
        public InteractiveController(ILogger<InteractiveController> logger)
        {
            this.logger = logger;
        }

        private readonly ILogger logger;
        public record UploadModel(Dictionary<string, string> Data, string Uid);


        [HttpPost("[Action]")]
        public IActionResult UploadData([FromBody]UploadModel request)
        {
            this.logger.LogInformation("数据上传\nId:{uid}\nContent:{json}", request.Uid, request.Data);
            if (request.Data.ContainsKey("车速"))
            {
                if (float.Parse(request.Data["车速"]) > 100)
                {
                    this.logger.LogInformation("触发器：{type},{contiditon},{relatedObject}", "数值触发器", "车速大于100", request.Uid);
                    this.logger.LogWarning("车速过快：{relatedObject},当前值{value}", request.Uid, float.Parse(request.Data["车速"]));
                }
            }
            return Ok();
        }
    }
}
