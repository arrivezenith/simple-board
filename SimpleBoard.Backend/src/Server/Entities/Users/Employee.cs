﻿using System;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace SimpleBoard.Backend.Server.Entities.Users
{
    public class Employee
    {
        [Key, DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public Guid Id { get; set; }

        /// <summary>
        /// 员工工号
        /// </summary>
        public int EmployeeId { get; set; }

        /// <summary>
        /// 员工姓名
        /// </summary>
        public string Name { get; set; } = null!;

        /// <summary>
        /// 联系方式
        /// </summary>
        public string Contact { get; set; } = null!;

        /// <summary>
        /// 邮箱
        /// </summary>
        public string Email { get; set; } = null!;
    }
}
