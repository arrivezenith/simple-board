﻿using Microsoft.AspNetCore.Mvc;
using SimpleBoard.Backend.Server.Models;

namespace SimpleBoard.Backend.Server.Extensions
{
    public static class ControllerExtension
    {
        public static IActionResult Success<T>(this ControllerBase controller, string message, T data)
        { 
            return controller.Ok(new ApiResult<T>
            {
                Code = 0,
                Message = message,
                Data = data
            });
        }

        public static IActionResult Success(this ControllerBase controller, string message)
        {
            return controller.Ok(new ApiResult<object>
            {
                Code = 0,
                Message = message,
                Data = null
            });
        }

        public static IActionResult Fail(this ControllerBase controller, string message)
        {
            return controller.Ok(new ApiResult<object>
            {
                Code = -1,
                Message = message,
                Data = null
            });
        }
    }
}
