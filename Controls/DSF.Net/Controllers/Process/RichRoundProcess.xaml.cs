﻿using DSF.Net.Controllers.Theme;
using SimpleBoard.ControllerMeta;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace DSF.Net.Controllers.Process
{
    /// <summary>
    /// RichRoundProcess.xaml 的交互逻辑
    /// </summary>
    [SimpleBoardController("DSF指示器-圆形范围文本")]
    [HasConfigurableProperty(nameof(Value), PropertyType.Double, "数值")]
    [HasConfigurableProperty(nameof(LimitDown), PropertyType.Double, "下限")]
    [HasConfigurableProperty(nameof(LimitUp), PropertyType.Double, "上限")]
    [HasConfigurableProperty(nameof(Text), PropertyType.String, "文本")]
    [HasConfigurableProperty(nameof(Precision), PropertyType.Int32, "保留小数")]
    public partial class RichRoundProcess : UserControl,IUserTheme
    {
        public RichRoundProcess()
        {
            InitializeComponent();
            DataContext = this;
            Width = 110;
            Height = 136;
        }


        public double Value
        {
            get { return (double)GetValue(ValueProperty); }
            set { SetValue(ValueProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Value.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ValueProperty =
            DependencyProperty.Register("Value", typeof(double), typeof(RichRoundProcess), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as RichRoundProcess;
                var oldValue = (double)e.OldValue;
                var newValue = (double)e.NewValue;
                var animation = new DoubleAnimation
                {
                    From = oldValue,
                    To = newValue,
                    Duration = new Duration(new TimeSpan(0, 0, 0, 0, 500)),
                    EasingFunction = new QuadraticEase()
                };
                c.BeginAnimation(AnimateProperty, animation);
            }));

        public double LimitDown
        {
            get { return (double)GetValue(LimitDownProperty); }
            set { SetValue(LimitDownProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LimitDown.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LimitDownProperty =
            DependencyProperty.Register("LimitDown", typeof(double), typeof(RichRoundProcess), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as RichRoundProcess;
                c.labelRange.Content = $"({e.NewValue}-{c.LimitUp})";
                c.arc.EndAngle = (c.Value - c.LimitDown) / (c.LimitUp - (double)e.NewValue) * 360;
            }));

        public double LimitUp
        {
            get { return (double)GetValue(LimitUpProperty); }
            set { SetValue(LimitUpProperty, value); }
        }

        // Using a DependencyProperty as the backing store for LimitUp.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty LimitUpProperty =
            DependencyProperty.Register("LimitUp", typeof(double), typeof(RichRoundProcess), new PropertyMetadata(100d, (s, e) =>
            {
                var c = s as RichRoundProcess;
                c.labelRange.Content = $"({c.LimitDown}-{e.NewValue})";
                c.arc.EndAngle = (c.Value - c.LimitDown) / ((double)e.NewValue - c.LimitDown) * 360;
            }));

        public string Text
        {
            get { return (string )GetValue(TextProperty); }
            set { SetValue(TextProperty, value); }
        }

        public int Precision
        {
            get { return (int)GetValue(PrecisionProperty); }
            set { SetValue(PrecisionProperty, value); }
        }

        // Using a DependencyProperty as the backing store for Precision.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty PrecisionProperty =
            DependencyProperty.Register("Precision", typeof(int), typeof(RichRoundProcess), new PropertyMetadata(3, (s, e) =>
            {
                var c = s as RichRoundProcess;
                var value = (int)e.NewValue;
                var priciseStr = new StringBuilder("#0.");
                for (var i = 0; i < value; i++)
                {
                    priciseStr.Append('0');
                }
                c.labelValue.Content = c.Value.ToString(priciseStr.ToString());
            }));

        // Using a DependencyProperty as the backing store for Text.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty TextProperty =
            DependencyProperty.Register("Text", typeof(string), typeof(RichRoundProcess), new PropertyMetadata("Process(%)", (s, e) =>
            {
                var c = s as RichRoundProcess;
                c.labelTitle.Content = e.NewValue;
            }));

        private static readonly DependencyProperty AnimateProperty =
            DependencyProperty.Register("Animate", typeof(double), typeof(RichRoundProcess), new PropertyMetadata(0d, (s, e) =>
            {
                var c = s as RichRoundProcess;
                var value = (double)e.NewValue;
                c.arc.EndAngle = (value - c.LimitDown) / (c.LimitUp - c.LimitDown) * 360;
                var priciseStr = new StringBuilder("#0.");
                for (var i = 0; i < c.Precision; i++)
                {
                    priciseStr.Append('0');
                }
                c.labelValue.Content = value.ToString(priciseStr.ToString());
            }));
        public Theme.Colors ThemeColor
        {
            get { return (Theme.Colors)GetValue(ThemeColorProperty); }
            set { SetValue(ThemeColorProperty, value); }
        }

        // Using a DependencyProperty as the backing store for ThemeColor.  This enables animation, styling, binding, etc...
        public static readonly DependencyProperty ThemeColorProperty =
            DependencyProperty.Register("ThemeColor", typeof(Theme.Colors), typeof(RichRoundProcess), new PropertyMetadata(Theme.Colors.Default));

    }
}
