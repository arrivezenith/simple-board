﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SimpleBoard.DependencyInjection
{
    [AttributeUsage(AttributeTargets.Class)]
    public class InjectAsAttribute : Attribute
    {
        public InjectAsAttribute(InjectionType injectionType)
        {
            InjectionType = injectionType;
        }

        public InjectionType InjectionType { get; set; }
    }
}
